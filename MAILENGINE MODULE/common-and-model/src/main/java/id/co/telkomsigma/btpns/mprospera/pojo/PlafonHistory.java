package id.co.telkomsigma.btpns.mprospera.pojo;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.Id;
import javax.persistence.SqlResultSetMapping;

@SqlResultSetMapping(name = "PlafonHistory", entities = {
		@EntityResult(entityClass = PlafonHistory.class, fields = {
				@FieldResult(name = "loanId",column = "id"),
				@FieldResult(name = "plafond",column = "plafond"),
		}) })
@Entity
public class PlafonHistory {
	@Id
	private Long loanId;
	private BigDecimal plafond;
	
	public Long getLoanId() {
		return loanId;
	}
	public void setLoanId(Long loanId) {
		this.loanId = loanId;
	}
	public BigDecimal getPlafond() {
		return plafond;
	}
	public void setPlafond(BigDecimal plafond) {
		this.plafond = plafond;
	}
	
	
}
