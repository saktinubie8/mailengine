package id.co.telkomsigma.btpns.mprospera.request;

public class PasswordResetRequest extends BaseRequest {

    private String username;
    private String sessionKey;
    private String imei;
    private String oldPassword;
    private String newPassword;
    private String tokenChallenge;
    private String tokenResponse;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getSessionKey() {
        return sessionKey;
    }

    public void setSessionKey(String sessionKey) {
        this.sessionKey = sessionKey;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getTokenResponse() {
        return tokenResponse;
    }

    public void setTokenResponse(String tokenResponse) {
        this.tokenResponse = tokenResponse;
    }

    public String getTokenChallenge() {
        return tokenChallenge;
    }

    public void setTokenChallenge(String tokenChallenge) {
        this.tokenChallenge = tokenChallenge;
    }

    @Override
    public String toString() {
        return "PasswordResetRequest [username=" + username + ", sessionKey=" + sessionKey + ", imei=" + imei
                + ", oldPassword=" + oldPassword + ", newPassword=" + newPassword + ", tokenChallange="
                + tokenChallenge + ", tokenResponse=" + tokenResponse + ", getTransmissionDateAndTime()="
                + getTransmissionDateAndTime() + ", getRetrievalReferenceNumber()=" + getRetrievalReferenceNumber()
                + "]";
    }

}