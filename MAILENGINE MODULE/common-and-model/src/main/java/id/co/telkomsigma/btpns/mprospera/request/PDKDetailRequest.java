package id.co.telkomsigma.btpns.mprospera.request;

public class PDKDetailRequest {

    private String swId;
    private String graduateStatus;

    public String getSwId() {
        return swId;
    }

    public void setSwId(String swId) {
        this.swId = swId;
    }

    public String getGraduateStatus() {
        return graduateStatus;
    }

    public void setGraduateStatus(String graduateStatus) {
        this.graduateStatus = graduateStatus;
    }

}