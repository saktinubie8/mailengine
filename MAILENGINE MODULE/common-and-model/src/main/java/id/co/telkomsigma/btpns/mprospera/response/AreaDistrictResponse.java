package id.co.telkomsigma.btpns.mprospera.response;

import java.util.ArrayList;
import java.util.List;

public class AreaDistrictResponse {

    private String areaName;
    private String areaId;
    private String parentAreaId;
    private String postalCode;

    private List<AreaKecamatanResponse> areaListKecamatan;

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public String getAreaId() {
        return areaId;
    }

    public void setAreaId(String areaId) {
        this.areaId = areaId;
    }

    public String getParentAreaId() {
        return parentAreaId;
    }

    public void setParentAreaId(String parentAreaId) {
        this.parentAreaId = parentAreaId;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public List<AreaKecamatanResponse> getAreaListKecamatan() {
        if (areaListKecamatan == null)
            areaListKecamatan = new ArrayList<>();
        return areaListKecamatan;
    }

    public void setAreaListKecamatan(List<AreaKecamatanResponse> areaListKecamatan) {
        this.areaListKecamatan = areaListKecamatan;
    }

}