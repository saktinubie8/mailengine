package id.co.telkomsigma.btpns.mprospera.request;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * Created by Dzulfiqar on 25/04/2017.
 */

public class SWApprovalHistoryReq {

    private String date;
    private String name;
    private String role;
    private String supervisorId;
    private String swId;
    private String level;
    private String limit;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getSupervisorId() {
        return supervisorId;
    }

    public void setSupervisorId(String supervisorId) {
        this.supervisorId = supervisorId;
    }

    public String getSwId() {
        return swId;
    }

    public void setSwId(String swId) {
        this.swId = swId;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getLimit() {
        return limit;
    }

    public void setLimit(String limit) {
        this.limit = limit;
    }

}