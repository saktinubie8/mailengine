package id.co.telkomsigma.btpns.mprospera.pojo;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.Id;
import javax.persistence.SqlResultSetMapping;

@SqlResultSetMapping(name = "SwPreviousIncome", entities = {
		@EntityResult(entityClass = SwPreviousIncome.class, fields = {
				@FieldResult(name = "id",column = "id"),
				@FieldResult(name = "totalIncome",column = "total_income"),

		}) })


@Entity
public class SwPreviousIncome {

	@Id
	private Long id;
	private BigDecimal totalIncome;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public BigDecimal getTotalIncome() {
		return totalIncome;
	}
	public void setTotalIncome(BigDecimal totalIncome) {
		this.totalIncome = totalIncome;
	}
	
	

	
	
}
