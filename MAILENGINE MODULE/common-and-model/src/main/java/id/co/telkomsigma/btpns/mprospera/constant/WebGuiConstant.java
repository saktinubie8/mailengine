package id.co.telkomsigma.btpns.mprospera.constant;

/**
 * Created by daniel on 4/17/15.
 */
public final class WebGuiConstant {

    /**
     * URL GLOBAL
     */
    public static final String TERMINAL_ECHO_PATH = "/webservice/echo**";

    /**
     * RESPONSE CODE
     */
    public static final String RC_SUCCESS = "00";
    public static final String RC_FAILED_LOGIN = "01";
    public static final String RC_RESET_PASSWORD_FAILED = "02";
    public static final String RC_USER_DISABLE = "03";
    public static final String RC_TERMINAL_NOT_FOUND = "04";
    public static final String RC_GLOBAL_EXCEPTION = "05";
    public static final String RC_ERROR_SCORING = "06";
    public static final String RC_ROLE_CANNOT_ACCESS_MOBILE = "07";
    public static final String RC_INVALID_SDA_CATEGORY = "08";
    public static final String RC_KFO_CUT_OFF = "09";
    public static final String RC_CREATE_SW_FAILED = "10";
    public static final String RC_SW_ALREADY_EXIST = "11";
    public static final String RC_DELETE_SW_FAILED = "12";
    public static final String RC_CIF_NOT_FOUND = "13";
    public static final String RC_UPDATE_SW_FAILED = "14";
    public static final String RC_INVALID_APK = "15";
    public static final String RC_UNKNOWN_MM_ID = "16";
    public static final String RC_UNKNOWN_PM_ID = "17";
    public static final String RC_UNKNOWN_LOAN_ID = "18";
    public static final String RC_UNKNOWN_SURVEY_ID = "19";
    public static final String RC_CREATE_SENTRA_FAILED = "20";
    public static final String RC_SENTRA_ALREADY_EXIST = "21";
    public static final String RC_UPDATE_SENTRA_FAILED = "22";
    public static final String RC_SENTRA_ID_NOT_FOUND = "23";
    public static final String RC_ASSIGN_PS_ERROR = "24";
    public static final String RC_CREATE_GROUP_FAILED = "25";
    public static final String RC_GROUP_ALREADY_EXIST = "26";
    public static final String RC_DELETE_GROUP_FAILED = "27";
    public static final String RC_UPDATE_GROUP_FAILED = "28";
    public static final String RC_GROUP_ID_NOT_FOUND = "29";
    public static final String RC_CREATE_CUSTOMER_FAILED = "30";
    public static final String RC_CUSTOMER_ALREADY_EXIST = "31";
    public static final String RC_TRANSACTION_INQUIRY_ERROR = "32";
    public static final String RC_SENTRAGROUP_NOT_APPROVED = "33";
    public static final String RC_SENTRAGROUP_NULL = "34";
    public static final String RC_CREATE_LOAN_FAILED = "35";
    public static final String RC_LOAN_ALREADY_EXIST = "36";
    public static final String RC_DELETE_LOAN_FAILED = "37";
    public static final String RC_APPROVAL_DEVIATION_FAILED = "38";
    public static final String RC_CLAIM_INSURANCE_FAILED = "39";
    public static final String RC_SUBMIT_PRS_FAILED = "40";
    public static final String RC_INQUIRY_PRS_ERROR = "41";
    public static final String RC_ID_PHOTO_NULL = "42";
    public static final String RC_SURVEY_PHOTO_NULL = "43";
    public static final String RC_PRS_REJECTED = "44";
    public static final String RC_UNKNOWN_SAVING = "45";
    public static final String RC_NO_LOAN_HIST = "46";
    public static final String RC_NO_SAVING_HIST = "47";
    public static final String RC_INVALID_KECAMATAN = "48";
    public static final String RC_INVALID_KELURAHAN = "49";
    public static final String RC_SUBMIT_COLLECTION_FAILED = "50";
    public static final String RC_AP3R_ALREADY_EXIST = "51";
    public static final String RC_SW_PRODUCT_NULL = "52";
    public static final String RC_SW_APPROVED_FAILED = "53";
    public static final String RC_PHOTO_ALREADY_EXIST = "54";
    public static final String RC_USER_CANNOT_APPROVE = "55";
    public static final String RC_UNKNOWN_INSURANCE_CLAIM_ID = "56";
    public static final String RC_FILE_DAYA_NULL = "57";
    public static final String RC_EARLY_TERMINATION_PLAN_NULL = "58";
    public static final String RC_UNREGISTERED_USER = "59";
    public static final String RC_CUSTOMER_WAIT_APPROVED = "60";
    public static final String RC_LOCATION_ID_NOT_FOUND = "61";
    public static final String RC_APPID_NULL = "62";
    public static final String RC_GROUP_LEADER_NULL = "63";
    public static final String RC_INVALID_RRN = "64";
    public static final String RC_AP3R_NULL = "65";
    public static final String RC_DEVIATION_NOT_APPROVED = "66";
    public static final String RC_DEVIATION_NULL = "67";
    public static final String RC_SENTRA_PHOTO_NULL = "68";
    public static final String RC_SW_NOT_APPROVED = "69";
    public static final String RC_DEVIATION_CANNOT_DELETE = "70";
    public static final String RC_EOD_PROSPERA_IS_RUNNING = "91";
    public static final String RC_GENERATE_AP3R_FAILED = "A9";
    public static final String RC_ESB_UNAVAILABLE = "SU";
    public static final String RC_INVALID_SESSION_KEY = "B1";
    public static final String RC_UNKNOWN_PARENT_AREA = "B2";
    public static final String RC_UNKNOWN_SDA_ID = "B3";
    public static final String RC_UNKNOWN_SENTRA_ID = "B4";
    public static final String RC_UNKNOWN_PDK_ID = "B5";
    public static final String RC_UNKNOWN_SW_ID = "B6";
    public static final String RC_UNKNOWN_CUSTOMER_ID = "B7";
    public static final String RC_USERNAME_NOT_EXISTS = "B8";
    public static final String RC_IMEI_NOT_EXISTS = "B9";
    public static final String RC_SESSION_KEY_NOT_EXISTS = "C1";
    public static final String RC_IMEI_NOT_VALID_FORMAT = "C2";
    public static final String RC_USER_INVALID_CITY = "C3";
    public static final String RC_SENTRA_ALREADY_APPROVED = "C4";
    public static final String RC_UNKNOWN_STATUS = "C5";
    public static final String RC_ALREADY_CLOSED = "C6";
    public static final String RC_ALREADY_REJECTED = "C7";
    public static final String RC_ALREADY_CANCEL = "C8";
    public static final String RC_SENTRA_NOT_APPROVED = "C9";
    public static final String RC_UNKNOWN_APPID = "D1";
    public static final String RC_USERNAME_USED = "D2";
    public static final String RC_UNKNOWN_CIF_NUMBER = "D3";
    public static final String RC_PRS_NULL = "D4";
    public static final String RC_PRS_PHOTO_NULL = "D5";
    public static final String RC_PRS_ALREADY_SUBMIT = "D6";
    public static final String RC_UNKNOWN_LOAN_PRS = "D7";
    public static final String RC_AP3R_ALREADY_APPROVED = "D8";
    public static final String RC_ROLE_NULL = "D9";
    public static final String RC_SW_ALREADY_APPROVED = "E1";
    public static final String RC_RECORD_ALREADY_APPROVED = "E2";
    public static final String DUPLICATE_PPI = "E3";
    public static final String AP3R_NOT_APPROVED = "E4";
    public static final String PPI_NULL = "E5";
    public static final String PPI_NOT_DONE = "E6";
    public static final String UPDATE_PPI_NOT_PERMITTED = "E7";
    public static final String RC_MODIFIED_BY_NOT = "E8";
    public static final String RC_UNKNOWN_PHOTO_SW_ID = "P1";
    public static final String RC_SERVER_TIMEOUT = "ZZ";
    public static final String RC_INVALID_USER_PASSWORD = "1A";
    public static final String RC_USER_LOCKED = "1B";
    public static final String RC_USER_INACTIVE = "1C";
    public static final String RC_GENERAL_ERROR = "XX";

    public static final String SDA_PROVINSI = "PROVINSI";
    public static final String SDA_KABUPATEN = "KABUPATEN";
    public static final String SDA_KECAMATAN = "KECAMATAN";
    public static final String SDA_KELURAHAN = "KELURAHAN";

    /**
     * URL WebService Ambil Foto KTP
     */
    public static final String TERMINAL_GET_ID_PHOTO = "/webservice/getIdPhoto**";
    public static final String TERMINAL_GET_ID_PHOTO_REQUEST = TERMINAL_GET_ID_PHOTO
            + "/{apkVersion:.+}";

    public static final String TERMINAL_GET_ID_PHOTO_LINK_REQUEST = TERMINAL_GET_ID_PHOTO_REQUEST
            + "/{id:.+}";

    /**
     * URL WebService Ambil Foto Tempat Usaha
     */
    public static final String TERMINAL_GET_BUSINESS_PLACE_PHOTO = "/webservice/getBusinessPlacePhoto**";
    public static final String TERMINAL_GET_BUSINESS_PLACE_PHOTO_REQUEST = TERMINAL_GET_BUSINESS_PLACE_PHOTO
            + "/{apkVersion:.+}";

    public static final String TERMINAL_BUSINESS_PLACE_PHOTO_LINK_REQUEST = TERMINAL_GET_BUSINESS_PLACE_PHOTO_REQUEST
            + "/{id:.+}";

    public static final String CLEAR_ALL_CACHE = "/clearCache**";
    
    /**
     * URL Webservice send email
     */
    public static final String SEND_EMAIL = "/webservice/sendEmail";
    public static final String SEND_EMAIL_REQUEST = SEND_EMAIL;
    public static final String SEND_NOTIF = "/webservice/sendNotif";
    public static final String SEND_NOTIF_REQUEST = SEND_NOTIF;
    public static final String SEND_NOTIF_REJECT_SCORING_REQUEST = "/webservice/sendNotifReject";
    public static final String SEND_NOTIF_DATA_NOT_COMPLETE = "/webservice/sendNotifNotComplete";
    public static final String LINK_VERIFIED = "/webservice/verLink";
    public static final String LINK_FORWARDED = "/webservice/fwdLink";
    
    /**
     * Mail body path  
     */
    public static final String MAIL_FROM = "nendikus@gmail.com";
    public static final String MAIL_PATH = "content/mail";
    public static final String MAIL_TEMPLATE_PATH = MAIL_PATH +"/mailtemplatenew";
    public static final String MAIL_NOTIF_TEMPLATE_PATH = MAIL_PATH + "/mailnotiftemplate";
    public static final String MAIL_NOTIF_REJECT_SCORING_TEMPLATE_PATH = MAIL_PATH + "/mailnotiftemplateco";
    public static final String MAIL_NOTIF_REJECT_SCORING_BM_TEMPLATE_PATH = MAIL_PATH + "/mailnotiftemplatebm";
    public static final String MAIL_NOTIF_RETURN_TEMPLATE_PATH = MAIL_PATH+"/mailtemplatereturn";
    public static final String MAIL_SUCCESS_PAGE_PATH = MAIL_PATH +"/success";
    public static final String MAIL_REJECT_REASON_PATH = MAIL_PATH +"/rejectreason";
    public static final String MAIL_REJECT_SUCCESS_PATH = MAIL_PATH + "/rejectsuccess";
    public static final String MAIL_FWD_SUCCESS_PAGE_PATH = MAIL_PATH+"/fwdsuccess";
    public static final String MAIL_FORWARD_PATH = MAIL_PATH+"/forward";
    public static final String MAIL_RETURN_PATH = MAIL_PATH+"/returnpage";
    public static final String MAIL_RETURN_REASON_PATH = MAIL_PATH+"/reason";
    public static final String MAIL_RETURN_NOTIF_PATH = MAIL_PATH+"/notifreturntemplate";
    public static final String MAIL_ERROR_GENERAL_PAGE_PATH = MAIL_PATH +"/error";
    public static final String MAIL_ERROR_ALREADY_APPROVED_PATH = MAIL_PATH+"/errorapproved";
    public static final String MAIL_ERROR_IMG_NOT_PROCESSED_PATH = MAIL_PATH +"/errorimg";
    public static final String MAIL_APPROVE_CONFIRM_PATH= MAIL_PATH+"/confirmapprove";
    
    /**
     * error for mail engine
     */
    public static final String MAIL_TOKEN_EXPIRED = "Token Expired";
    public static final String MAIL_UNKNOWN_STATUS = "Status tidak diketahui";
    public static final String MAIL_INVALID_TOKEN = "Token invalid / not found";
    public static final String MAIL_INVALID_ADDRESS = "Mail address invalid / null";
    public static final String MAIL_USERNAME_NOT_FOUND = "username not found";
    
    /**
     * Parameter for Mail
     */
    public static final String MAIL_BODY_FORWARDED = "fwdBody";
    public static final String MAIL_BODY = "valBody";
    public static final String MAIL_SUBJECT_FORWARDED = "fwdSubject";
    public static final String MAIL_SUBJECT = "valSubject";
    public static final String MAIL_SUBJECT_NOTIF = "notifSubject";
    public static final String MAIL_SUBJECT_RETURN = "returnSubject";
    public static final String MAIL_DATE_FORMAT = "dateformat";
    public static final String MAIL_DATE_EXPIRED = "expiredin";
    public static final String MAIL_SYSTEM = "System";
    public static final String MAIL_DOMAIN = "domain";
    
    /**
     * Constant Tempat Usaha
     */
    public static final String MILIK_PRIBADI = "Milik Pribadi";
    public static final String MILIK_ORANG_TUA = "Milik Orang Tua";
    public static final String MILIK_PIHAK_KETIGA = "Milik Pihak Ketiga";
    public static final String MILIK_LAINNYA = "Status tempat usaha lain nya";    
    
    /**
     * Constant STATUS
     */
    public static final String STATUS_DRAFT = "DRAFT";
    public static final String STATUS_APPROVED = "APPROVED";
    public static final String STATUS_ACTIVE = "ACTIVE";
    public static final String STATUS_SUBMIT = "SUBMIT";
    public static final String STATUS_CLOSED = "CLOSED";
    public static final String STATUS_REJECTED = "REJECTED";
    public static final String STATUS_CANCEL = "CANCEL";
    public static final String STATUS_RETURNED = "RETURNED";
    public static final String STATUS_NA = "N/A";
    public static final String STATUS_DONE = "DONE";
    public static final String STATUS_APPROVED_MS = "APPROVED-MS";
    public static final String STATUS_PLAN = "PLAN";
    public static final String STATUS_WAITING_APPROVAL = "WAITING FOR APPROVAL";
    public static final String STATUS_EXPIRED = "EXPIRED";
    public static final String STATUS_FORWARDED = "FORWARDED";

    /**
     * Constant action
     */
    public static final String ACTION_INSERT = "insert";
    public static final String ACTION_UPDATE = "update";
    public static final String ACTION_DELETE = "delete";

}