package id.co.telkomsigma.btpns.mprospera.request;

public class ProsperaLoginRequest extends BaseRequest {

    private String username;
    private String password;
    private String imei;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    @Override
    public String toString() {
        return "ProsperaLoginRequest [username=" + username + ", password=" + password + ", imei=" + imei
                + ", getTransmissionDateAndTime()=" + getTransmissionDateAndTime() + ", getRetrievalReferenceNumber()="
                + getRetrievalReferenceNumber() + "]";
    }

}