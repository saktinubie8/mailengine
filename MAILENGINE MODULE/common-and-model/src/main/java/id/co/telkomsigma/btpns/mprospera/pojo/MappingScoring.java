package id.co.telkomsigma.btpns.mprospera.pojo;

import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.Id;
import javax.persistence.SqlResultSetMapping;

@SqlResultSetMapping(name = "MappingScoring", entities = {
		@EntityResult(entityClass = MappingScoring.class, fields = {
				@FieldResult(name = "id",column = "id"),
				@FieldResult(name = "transaksiBaliout",column = "transaksi_bailout"),
				@FieldResult(name = "frekuensiAbsen",column = "frekuensi_absen"),
		}) })

@Entity
public class MappingScoring {
	
	@Id
	private long id;
	private int transaksiBaliout;
	private int frekuensiAbsen;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public int getTransaksiBaliout() {
		return transaksiBaliout;
	}
	public void setTransaksiBaliout(int transaksiBaliout) {
		this.transaksiBaliout = transaksiBaliout;
	}
	public int getFrekuensiAbsen() {
		return frekuensiAbsen;
	}
	public void setFrekuensiAbsen(int frekuensiAbsen) {
		this.frekuensiAbsen = frekuensiAbsen;
	}
	
	
	

}
