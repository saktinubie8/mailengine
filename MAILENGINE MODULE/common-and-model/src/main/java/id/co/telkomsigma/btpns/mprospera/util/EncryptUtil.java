package id.co.telkomsigma.btpns.mprospera.util;

import java.security.Key;
import java.security.MessageDigest;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;

public class EncryptUtil {

    private static final String ALGO = "AES";
    private static final byte[] keyValue = new byte[]{'M', 'x', 'E', 's', 'B', 't', '3', 'L', 'k', '0', 'm', 's', '1',
            'G', 'm', 'A'};

    public static String encrypt(String Data) {
        try {
            Key key;
            key = generateKey();
            Cipher c = Cipher.getInstance(ALGO);
            c.init(Cipher.ENCRYPT_MODE, key);
            byte[] encVal = c.doFinal(Data.getBytes("UTF-8"));
            String encryptedValue = Base64.encodeBase64String(encVal);
            return encryptedValue;
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }

    public static String decrypt(String encryptedData) {
        try {
            Key key;
            key = generateKey();
            Cipher c = Cipher.getInstance(ALGO);
            c.init(Cipher.DECRYPT_MODE, key);
            byte[] decordedValue = Base64.decodeBase64(encryptedData);
            byte[] decValue = c.doFinal(decordedValue);
            String decryptedValue = new String(decValue);
            return decryptedValue;
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }

    private static Key generateKey() throws Exception {
        MessageDigest sha1 = MessageDigest.getInstance("SHA-256");
        String testKey = "THISISKEYFORCRYPTO";
        byte[] digest = sha1.digest(testKey.getBytes("UTF-8"));
        Key key = new SecretKeySpec(digest, "AES");
        return key;
    }

    public static void main(String args[]) {
        String password = "P@ssw0rd";
        try {
            String enkrip = EncryptUtil.encrypt(password);
            String dekrip = EncryptUtil.decrypt(enkrip);

            System.out.println(enkrip);
            System.out.println(dekrip);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

}