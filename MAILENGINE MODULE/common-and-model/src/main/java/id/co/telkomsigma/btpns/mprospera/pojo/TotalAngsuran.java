package id.co.telkomsigma.btpns.mprospera.pojo;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.Id;
import javax.persistence.SqlResultSetMapping;

@SqlResultSetMapping(name = "TotalAngsuran", entities = {
		@EntityResult(entityClass = TotalAngsuran.class, fields = {
				@FieldResult(name = "loanPRSId",column = "id"),
				@FieldResult(name = "totalAngsuran",column = "total_angsuran"),
		}) })

@Entity
public class TotalAngsuran {
	@Id
	private Long loanPRSId;
	private BigDecimal totalAngsuran;

	
	public Long getLoanPRSId() {
		return loanPRSId;
	}
	public void setLoanPRSId(Long loanPRSId) {
		this.loanPRSId = loanPRSId;
	}
	public BigDecimal getTotalAngsuran() {
		return totalAngsuran;
	}
	public void setTotalAngsuran(BigDecimal totalAngsuran) {
		this.totalAngsuran = totalAngsuran;
	}

	
	
	
	   
	
}
