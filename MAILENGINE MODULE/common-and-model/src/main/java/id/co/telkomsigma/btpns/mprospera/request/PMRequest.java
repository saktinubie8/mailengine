package id.co.telkomsigma.btpns.mprospera.request;

import java.util.List;

public class PMRequest extends BaseRequest {

    /* untuk add PM */
    private String transmissionDateAndTime;
    private String retrievalReferenceNumber;
    private String username;
    private String sessionKey;
    private String imei;
    private String areaId;
    private String pmLocationName;
    private String pmOwner;
    private String pmParticipant;
    private String longitude;
    private String latitude;
    private String action;
    private String pmId;
    private String pmOwnerPhoneNumber;
    private String time;
    private String pmDate;
    private String pmPlanDate;
    private String swCandidate;
    private String localId;
    private List<SWRequest> swPlanList;
    /* untuk list PM */
    private Integer getCountData;
    private String lastPmId;
    private String startLookupDate;
    private String endLookupDate;
    private Integer page;
    private String tokenKey;

    @Override
    public String toString() {
        return "PMRequest{" +
                "transmissionDateAndTime='" + transmissionDateAndTime + '\'' +
                ", retrievalReferenceNumber='" + retrievalReferenceNumber + '\'' +
                ", username='" + username + '\'' +
                ", sessionKey='" + sessionKey + '\'' +
                ", imei='" + imei + '\'' +
                ", areaId='" + areaId + '\'' +
                ", pmLocationName='" + pmLocationName + '\'' +
                ", pmOwner='" + pmOwner + '\'' +
                ", pmParticipant='" + pmParticipant + '\'' +
                ", longitude='" + longitude + '\'' +
                ", latitude='" + latitude + '\'' +
                ", action='" + action + '\'' +
                ", pmId='" + pmId + '\'' +
                ", pmOwnerPhoneNumber='" + pmOwnerPhoneNumber + '\'' +
                ", time='" + time + '\'' +
                ", pmDate='" + pmDate + '\'' +
                ", pmPlanDate='" + pmPlanDate + '\'' +
                ", swCandidate='" + swCandidate + '\'' +
                ", localId='" + localId + '\'' +
                ", swPlanList=" + swPlanList +
                ", getCountData=" + getCountData +
                ", lastPmId='" + lastPmId + '\'' +
                ", startLookupDate='" + startLookupDate + '\'' +
                ", endLookupDate='" + endLookupDate + '\'' +
                ", page=" + page +
                '}';
    }

    public String getLocalId() {
        return localId;
    }

    public void setLocalId(String localId) {
        this.localId = localId;
    }

    public String getTransmissionDateAndTime() {
        return transmissionDateAndTime;
    }

    public void setTransmissionDateAndTime(String transmissionDateAndTime) {
        this.transmissionDateAndTime = transmissionDateAndTime;
    }

    public String getRetrievalReferenceNumber() {
        return retrievalReferenceNumber;
    }

    public void setRetrievalReferenceNumber(String retrievalReferenceNumber) {
        this.retrievalReferenceNumber = retrievalReferenceNumber;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getSessionKey() {
        return sessionKey;
    }

    public void setSessionKey(String sessionKey) {
        this.sessionKey = sessionKey;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getAreaId() {
        return areaId;
    }

    public void setAreaId(String areaId) {
        this.areaId = areaId;
    }

    public String getPmLocationName() {
        return pmLocationName;
    }

    public void setPmLocationName(String pmLocationName) {
        this.pmLocationName = pmLocationName;
    }

    public String getPmOwner() {
        return pmOwner;
    }

    public void setPmOwner(String pmOwner) {
        this.pmOwner = pmOwner;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getPmId() {
        return pmId;
    }

    public void setPmId(String pmId) {
        this.pmId = pmId;
    }

    public Integer getGetCountData() {
        return getCountData;
    }

    public void setGetCountData(Integer getCountData) {
        this.getCountData = getCountData;
    }

    public String getLastPmId() {
        return lastPmId;
    }

    public void setLastPmId(String lastPmId) {
        this.lastPmId = lastPmId;
    }

    public String getStartLookupDate() {
        return startLookupDate;
    }

    public void setStartLookupDate(String startLookupDate) {
        this.startLookupDate = startLookupDate;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public String getEndLookupDate() {
        return endLookupDate;
    }

    public void setEndLookupDate(String endLookupDate) {
        this.endLookupDate = endLookupDate;
    }

    public String getPmOwnerPhoneNumber() {
        return pmOwnerPhoneNumber;
    }

    public void setPmOwnerPhoneNumber(String pmOwnerPhoneNumber) {
        this.pmOwnerPhoneNumber = pmOwnerPhoneNumber;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getPmDate() {
        return pmDate;
    }

    public void setPmDate(String pmDate) {
        this.pmDate = pmDate;
    }

    public List<SWRequest> getSwPlanList() {
        return swPlanList;
    }

    public void setSwPlanList(List<SWRequest> swPlanList) {
        this.swPlanList = swPlanList;
    }

    public String getSwCandidate() {
        return swCandidate;
    }

    public void setSwCandidate(String swCandidate) {
        this.swCandidate = swCandidate;
    }

    public String getPmParticipant() {
        return pmParticipant;
    }

    public void setPmParticipant(String pmParticipant) {
        this.pmParticipant = pmParticipant;
    }

    public String getPmPlanDate() {
        return pmPlanDate;
    }

    public void setPmPlanDate(String pmPlanDate) {
        this.pmPlanDate = pmPlanDate;
    }

    public String getTokenKey() {
        return tokenKey;
    }

    public void setTokenKey(String tokenKey) {
        this.tokenKey = tokenKey;
    }

}