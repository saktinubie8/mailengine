package id.co.telkomsigma.btpns.mprospera.request;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_EMPTY)
public class HistoryApprovalRequest {

	private String rejectedReason;
	private String status;
	private String swId;
	private String localId;
	private String imei;
	private String retrievalReferenceNumber;
	private String sessionKey;
	private String transmissionDateAndTime;
	private String username;
	public String getRejectedReason() {
		return rejectedReason;
	}
	public void setRejectedReason(String rejectedReason) {
		this.rejectedReason = rejectedReason;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getSwId() {
		return swId;
	}
	public void setSwId(String swId) {
		this.swId = swId;
	}
	public String getLocalId() {
		return localId;
	}
	public void setLocalId(String localId) {
		this.localId = localId;
	}
	public String getImei() {
		return imei;
	}
	public void setImei(String imei) {
		this.imei = imei;
	}
	public String getRetrievalReferenceNumber() {
		return retrievalReferenceNumber;
	}
	public void setRetrievalReferenceNumber(String retrievalReferenceNumber) {
		this.retrievalReferenceNumber = retrievalReferenceNumber;
	}
	public String getSessionKey() {
		return sessionKey;
	}
	public void setSessionKey(String sessionKey) {
		this.sessionKey = sessionKey;
	}
	public String getTransmissionDateAndTime() {
		return transmissionDateAndTime;
	}
	public void setTransmissionDateAndTime(String transmissionDateAndTime) {
		this.transmissionDateAndTime = transmissionDateAndTime;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	
	
}
