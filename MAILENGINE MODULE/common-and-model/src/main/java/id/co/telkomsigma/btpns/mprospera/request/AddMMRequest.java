package id.co.telkomsigma.btpns.mprospera.request;

import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@SuppressWarnings("ALL")
@JsonSerialize(include = JsonSerialize.Inclusion.NON_EMPTY)
public class AddMMRequest extends BaseRequest {

    private String username;
    private String sessionKey;
    private String imei;
    private String areaId;
    private String mmLocationName;
    private String mmOwner;
    private String mmParticipant;
    private String longitude;
    private String latitude;
    private String action;
    private String mmId;
    private String pmCandidate;
    private String localId;
    private List<PMRequest> pmPlanList;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getSessionKey() {
        return sessionKey;
    }

    public void setSessionKey(String sessionKey) {
        this.sessionKey = sessionKey;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getAreaId() {
        return areaId;
    }

    public void setAreaId(String areaId) {
        this.areaId = areaId;
    }

    public String getMmLocationName() {
        return mmLocationName;
    }

    public void setMmLocationName(String mmLocationName) {
        this.mmLocationName = mmLocationName;
    }

    public String getMmOwner() {
        return mmOwner;
    }

    public void setMmOwner(String mmOwner) {
        this.mmOwner = mmOwner;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getMmId() {
        return mmId;
    }

    public void setMmId(String mmId) {
        this.mmId = mmId;
    }

    public String getPmCandidate() {
        return pmCandidate;
    }

    public void setPmCandidate(String pmCandidate) {
        this.pmCandidate = pmCandidate;
    }

    public List<PMRequest> getPmPlanList() {
        return pmPlanList;
    }

    public void setPmPlanList(List<PMRequest> pmPlanList) {
        this.pmPlanList = pmPlanList;
    }

    public String getMmParticipant() {
        return mmParticipant;
    }

    public void setMmParticipant(String mmParticipant) {
        this.mmParticipant = mmParticipant;
    }

    public String getLocalId() {
        return localId;
    }

    public void setLocalId(String localId) {
        this.localId = localId;
    }

    @Override
    public String toString() {
        return "AddMMRequest{" +
                "username='" + username + '\'' +
                ", sessionKey='" + sessionKey + '\'' +
                ", imei='" + imei + '\'' +
                ", areaId='" + areaId + '\'' +
                ", mmLocationName='" + mmLocationName + '\'' +
                ", mmOwner='" + mmOwner + '\'' +
                ", mmParticipant='" + mmParticipant + '\'' +
                ", longitude='" + longitude + '\'' +
                ", latitude='" + latitude + '\'' +
                ", action='" + action + '\'' +
                ", mmId='" + mmId + '\'' +
                ", pmCandidate='" + pmCandidate + '\'' +
                ", localId='" + localId + '\'' +
                ", pmPlanList=" + pmPlanList +
                '}';
    }

}