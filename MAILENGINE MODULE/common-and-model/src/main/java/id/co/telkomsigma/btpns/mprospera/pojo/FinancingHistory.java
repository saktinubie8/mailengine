package id.co.telkomsigma.btpns.mprospera.pojo;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.Id;
import javax.persistence.SqlResultSetMapping;

@SqlResultSetMapping(name = "FinancingHistory", entities = {
		@EntityResult(entityClass = FinancingHistory.class, fields = {
				@FieldResult(name = "loanId",column = "id"),
				@FieldResult(name = "appId",column = "application_id"),
				@FieldResult(name = "plafond",column = "plafond"),
				@FieldResult(name = "status",column = "status"),
				@FieldResult(name = "productName",column = "product_name"),
				@FieldResult(name = "tenorBulan",column = "tenor_bulan"),
		}) })


@Entity
public class FinancingHistory {

	@Id
	private Long loanId;
	private String appId;
	private BigDecimal plafond;
	private String status;
	private String productName;
	private Integer tenorBulan;

	
	public Long getLoanId() {
		return loanId;
	}
	public void setLoanId(Long loanId) {
		this.loanId = loanId;
	}
	public BigDecimal getPlafond() {
		return plafond;
	}
	public void setPlafond(BigDecimal plafond) {
		this.plafond = plafond;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public Integer getTenorBulan() {
		return tenorBulan;
	}
	public void setTenorBulan(Integer tenorBulan) {
		this.tenorBulan = tenorBulan;
	}

	public String getAppId() {
		return appId;
	}
	public void setAppId(String appId) {
		this.appId = appId;
	}
	
	
	
	
}
