package id.co.telkomsigma.btpns.mprospera.request;

public class RejectRequest extends BaseRequest{

	private String userName;
	private String customerName;
	private String mms;
	private String appidNo;
	private String sentra;
	private String alasan;
	
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getMms() {
		return mms;
	}
	public void setMms(String mms) {
		this.mms = mms;
	}
	public String getAppidNo() {
		return appidNo;
	}
	public void setAppidNo(String appidNo) {
		this.appidNo = appidNo;
	}
	public String getSentra() {
		return sentra;
	}
	public void setSentra(String sentra) {
		this.sentra = sentra;
	}
	public String getAlasan() {
		return alasan;
	}
	public void setAlasan(String alasan) {
		this.alasan = alasan;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	
}
