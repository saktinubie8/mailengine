package id.co.telkomsigma.btpns.mprospera.response;

import java.util.List;

/**
 * Created by Dzulfiqar on 12/04/2017.
 */
public class UserBwmpListResponse extends BaseResponse {

    private List<UserBwmpList> userBwmpList;

    public List<UserBwmpList> getUserBwmpList() {
        return userBwmpList;
    }

    public void setUserBwmpList(List<UserBwmpList> userBwmpList) {
        this.userBwmpList = userBwmpList;
    }

}