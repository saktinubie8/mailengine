package id.co.telkomsigma.btpns.mprospera.pojo;

import java.math.BigDecimal;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.Id;
import javax.persistence.SqlResultSetMapping;

@SqlResultSetMapping(name = "LimitTerpakai", entities = {
        @EntityResult(entityClass = LimitTerpakai.class, fields = {
                @FieldResult(name = "loanPRSId", column = "id"),
                @FieldResult(name = "remainingPrincipal", column = "remaining_principal"),
        })})
@Entity
public class LimitTerpakai {

    @Id
    private Long loanPRSId;
    private BigDecimal remainingPrincipal;

    public Long getLoanPRSId() {
        return loanPRSId;
    }

    public void setLoanPRSId(Long loanPRSId) {
        this.loanPRSId = loanPRSId;
    }

    public BigDecimal getRemainingPrincipal() {
        return remainingPrincipal;
    }

    public void setRemainingPrincipal(BigDecimal remainingPrincipal) {
        this.remainingPrincipal = remainingPrincipal;
    }

}