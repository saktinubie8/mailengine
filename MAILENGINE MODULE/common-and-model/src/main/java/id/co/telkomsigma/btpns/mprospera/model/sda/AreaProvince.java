package id.co.telkomsigma.btpns.mprospera.model.sda;

import java.util.List;

import javax.persistence.*;

import org.hibernate.annotations.OrderBy;

import id.co.telkomsigma.btpns.mprospera.model.GenericModel;

@Entity
@Table(name = "T_SDA_PROVINCE")
public class AreaProvince extends GenericModel {
    private static final long serialVersionUID = 6104317091168816L;

    private String areaId;
    private String areaCategory;
    private String areaName;
    private String postalCode;
    private List<AreaProvinceDetails> areaProvinceDetails;

    @OneToMany(fetch = FetchType.LAZY, targetEntity = AreaProvinceDetails.class)
    @JoinColumn(name = "area_id")
    @OrderBy(clause = "id asc")
    public List<AreaProvinceDetails> getAreaProvinceDetails() {
        return areaProvinceDetails;
    }

    public void setAreaProvinceDetails(List<AreaProvinceDetails> areaProvinceDetails) {
        this.areaProvinceDetails = areaProvinceDetails;
    }

    @Id
    @Column(name = "id", nullable = false, unique = true)
    public String getAreaId() {
        return areaId;
    }

    public void setAreaId(String areaId) {
        this.areaId = areaId;
    }

    @Transient
    public String getAreaCategory() {
        return areaCategory;
    }

    public void setAreaCategory(String areaCategory) {
        this.areaCategory = areaCategory;
    }

    @Column(name = "area_name", nullable = false)
    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    @Column(name = "postal_code")
    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

}