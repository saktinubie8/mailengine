package id.co.telkomsigma.btpns.mprospera.request;

public class ListMMRequest extends BaseRequest {

    private String username;
    private String sessionKey;
    private String imei;
    private Integer getCountData;
    private Integer page;
    private String startLookupDate;
    private String endLookupDate;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getSessionKey() {
        return sessionKey;
    }

    public void setSessionKey(String sessionKey) {
        this.sessionKey = sessionKey;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public Integer getGetCountData() {
        return getCountData;
    }

    public void setGetCountData(Integer getCountData) {
        this.getCountData = getCountData;
    }

    public String getStartLookupDate() {
        return startLookupDate;
    }

    public void setStartLookupDate(String startLookupDate) {
        this.startLookupDate = startLookupDate;
    }

    public String getEndLookupDate() {
        return endLookupDate;
    }

    public void setEndLookupDate(String endLookupDate) {
        this.endLookupDate = endLookupDate;
    }

    @Override
    public String toString() {
        return "ListMMRequest [username=" + username + ", sessionKey=" + sessionKey + ", imei=" + imei
                + ", getCountData=" + getCountData + ", page=" + page + ", startLookupDate=" + startLookupDate
                + ", endLookupDate=" + endLookupDate + ", getTransmissionDateAndTime()=" + getTransmissionDateAndTime()
                + ", getRetrievalReferenceNumber()=" + getRetrievalReferenceNumber() + "]";
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

}