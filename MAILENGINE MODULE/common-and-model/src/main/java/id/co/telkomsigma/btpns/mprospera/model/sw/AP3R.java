package id.co.telkomsigma.btpns.mprospera.model.sw;


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import id.co.telkomsigma.btpns.mprospera.model.GenericModel;

@Entity
@Table(name = "T_AP3R")
public class AP3R extends GenericModel {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private Long ap3rId;
    private Long swId;
    private String localId;
    private Character hasSaving;
    private String openSavingReason;
    private String fundSource;
    private Character transactionInYear;
    private String loanReason;
    private String businessField;
    private String subBusinessField;
    private Date disbursementDate;
    private Character highRiskCustomer;
    private Character highRiskBusiness;
    private String status;
    private Date createdDate;
    private String createdBy;
    private String updatedBy;
    private Date updatedDate;
    private String rejectedReason;
    private Date dueDate;
    private Long swProductId;
    private Integer approvalStatus;
    private Boolean isDeleted = false;
    private String longitude;
    private String latitude;

    @Id
    @Column(name = "id", nullable = false, unique = true)
    @GeneratedValue
    public Long getAp3rId() {
        return ap3rId;
    }

    public void setAp3rId(Long ap3rId) {
        this.ap3rId = ap3rId;
    }

    @Column(name = "sw_id")
    public Long getSwId() {
        return swId;
    }

    public void setSwId(Long swId) {
        this.swId = swId;
    }

    @Column(name = "has_saving")
    public Character getHasSaving() {
        return hasSaving;
    }

    public void setHasSaving(Character hasSaving) {
        this.hasSaving = hasSaving;
    }

    @Column(name = "open_saving_reason")
    public String getOpenSavingReason() {
        return openSavingReason;
    }

    public void setOpenSavingReason(String openSavingReason) {
        this.openSavingReason = openSavingReason;
    }

    @Column(name = "fund_source")
    public String getFundSource() {
        return fundSource;
    }

    public void setFundSource(String fundSource) {
        this.fundSource = fundSource;
    }

    @Column(name = "transaction_in_year")
    public Character getTransactionInYear() {
        return transactionInYear;
    }

    public void setTransactionInYear(Character transactionInYear) {
        this.transactionInYear = transactionInYear;
    }

    @Column(name = "loan_reason")
    public String getLoanReason() {
        return loanReason;
    }

    public void setLoanReason(String loanReason) {
        this.loanReason = loanReason;
    }

    @Column(name = "business_field")
    public String getBusinessField() {
        return businessField;
    }

    public void setBusinessField(String businessField) {
        this.businessField = businessField;
    }

    @Column(name = "sub_business_field")
    public String getSubBusinessField() {
        return subBusinessField;
    }

    public void setSubBusinessField(String subBusinessField) {
        this.subBusinessField = subBusinessField;
    }

    @Column(name = "disbursement_date")
    public Date getDisbursementDate() {
        return disbursementDate;
    }

    public void setDisbursementDate(Date disbursementDate) {
        this.disbursementDate = disbursementDate;
    }

    @Column(name = "high_risk_customer")
    public Character getHighRiskCustomer() {
        return highRiskCustomer;
    }

    public void setHighRiskCustomer(Character highRiskCustomer) {
        this.highRiskCustomer = highRiskCustomer;
    }

    @Column(name = "high_risk_business")
    public Character getHighRiskBusiness() {
        return highRiskBusiness;
    }

    public void setHighRiskBusiness(Character highRiskBusiness) {
        this.highRiskBusiness = highRiskBusiness;
    }

    @Column(name = "status")
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Column(name = "created_date")
    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    @Column(name = "created_by")
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Column(name = "üpdated_by")
    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    @Column(name = "updated_date")
    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    @Column(name = "rejected_reason")
    public String getRejectedReason() {
        return rejectedReason;
    }

    public void setRejectedReason(String rejectedReason) {
        this.rejectedReason = rejectedReason;
    }

    @Column(name = "sw_product_map_id")
    public Long getSwProductId() {
        return swProductId;
    }

    public void setSwProductId(Long swProductId) {
        this.swProductId = swProductId;
    }

    @Column(name = "due_date")
    public Date getDueDate() {
        return dueDate;
    }

    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

    @Column(name = "approval_status")
    public Integer getApprovalStatus() {
        return approvalStatus;
    }

    public void setApprovalStatus(Integer approvalStatus) {
        this.approvalStatus = approvalStatus;
    }

    @Column(name = "local_id")
    public String getLocalId() {
        return localId;
    }

    public void setLocalId(String localId) {
        this.localId = localId;
    }

    @Column(name = "is_deleted", nullable = true)
    public Boolean getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    @Column(name = "longitude")
    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    @Column(name = "latitude")
    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

}