package id.co.telkomsigma.btpns.mprospera.request;

/**
 * Created by Dzulfiqar on 12/04/2017.
 */
public class UserBwmpListRequest extends BaseRequest {

    private String username;
    private String imei;
    private String sessionKey;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getSessionKey() {
        return sessionKey;
    }

    public void setSessionKey(String sessionKey) {
        this.sessionKey = sessionKey;
    }

}