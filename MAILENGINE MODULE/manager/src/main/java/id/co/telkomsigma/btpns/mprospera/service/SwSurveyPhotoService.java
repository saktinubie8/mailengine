package id.co.telkomsigma.btpns.mprospera.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import id.co.telkomsigma.btpns.mprospera.manager.SwSurveyPhotoManager;
import id.co.telkomsigma.btpns.mprospera.model.sw.SwSurveyPhoto;

@Service("swSurveyPhotoService")
public class SwSurveyPhotoService {

    @Autowired
    private SwSurveyPhotoManager swSurveyPhotoManager;

    public SwSurveyPhoto getSwSurveyPhoto(Long swid) {
        return swSurveyPhotoManager.getSwSurveyPhoto(swid);
    }

}