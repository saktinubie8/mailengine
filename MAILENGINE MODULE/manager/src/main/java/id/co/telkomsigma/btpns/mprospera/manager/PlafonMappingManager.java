package id.co.telkomsigma.btpns.mprospera.manager;



import java.util.List;

import id.co.telkomsigma.btpns.mprospera.pojo.PlafonMapping;



public interface PlafonMappingManager {
	
	List<PlafonMapping> findByMonthlyInstallment(Integer installement,Integer tenor);

}
