package id.co.telkomsigma.btpns.mprospera.service;

import java.io.IOException;
import java.util.Date;
import java.util.Map;
import java.util.UUID;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.mail.util.ByteArrayDataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring4.SpringTemplateEngine;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hazelcast.util.Base64;
import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.dto.MailDto;
import id.co.telkomsigma.btpns.mprospera.dto.RejectDto;
import id.co.telkomsigma.btpns.mprospera.dto.ValidationDataDto;
import id.co.telkomsigma.btpns.mprospera.model.parameter.MailToken;
import id.co.telkomsigma.btpns.mprospera.model.user.User;

@Service("mailSenderService")
public class MailSenderService extends GenericService {

    @Autowired
    private JavaMailSender mailSender;

    @Autowired
    private SpringTemplateEngine templateEngine;

    @Autowired
    private MailTokenService mailTokenService;

    @Autowired
    private AP3RService AP3RSvc;

    @Autowired
    private UserService usrSvc;

    private static final String IMAGE_TYPE = "image/jpeg";
    private static final String IMAGE_KTP_FILENAME = "ktp.jpg";
    private static final String IMAGE_SUREY_FILENAME = "survey.jpg";

    public void sendEMail(MailDto mail, Boolean isForwarded, Date expiredDate, ValidationDataDto validationData) throws MessagingException, IOException {
        MimeMessage message = mailSender.createMimeMessage();
        String token = UUID.randomUUID().toString();
        MailToken mailToken = new MailToken();
        mailToken.setExpiredDate(expiredDate);
        mailToken.setIsExpired("0");
        mailToken.setToken(token);
        mailToken.setSwId(mail.getSwId());
        mailToken.setCreatedBy(mail.getUserName());
        mailToken.setCreatedDate(new Date());
        mailToken.setSendTo(mail.getTo());
        mailToken.setNote(validationData.getCatatan());
        mailToken.setStatus(WebGuiConstant.STATUS_WAITING_APPROVAL);
        mailToken.setInReview(validationData.getIsInReview());
        mailToken.setDeviationCode(validationData.getKodeDeviasi());
        mailTokenService.save(mailToken);
        Map<String, Object> mailData = castObjectToMap(validationData);
        //untuk deviasi
        String deviasi = getDeviationMessage(mail.getSwId(), validationData.getApprovalUsername(), validationData.getIsInReview());
        // set true if you want to include an attachment or using in line image
        MimeMessageHelper messageHelper = new MimeMessageHelper(message, true, "UTF-8");
        ByteArrayDataSource ktp = new ByteArrayDataSource(Base64.decode(mail.getKtpPhoto()), IMAGE_TYPE);
        ByteArrayDataSource survey = new ByteArrayDataSource(Base64.decode(mail.getSurveyPhoto()), IMAGE_TYPE);
        // Set variable for mail template
        Context context = new Context();
        context.setVariable("isForwarded", isForwarded);
        context.setVariable("token", token);
        context.setVariable("domain", mail.getDomain());
        context.setVariable("deviasiMsg", deviasi);
        context.setVariables(mailData);
        // Get mail template under resource folder
        String mailBody = templateEngine.process(WebGuiConstant.MAIL_TEMPLATE_PATH, context);
        // Set mail header and body
        String subject = mail.getSubject().replaceAll("\\bname\\b", validationData.getNamaNasabah());
        messageHelper.addAttachment(IMAGE_KTP_FILENAME, ktp);
        messageHelper.addAttachment(IMAGE_SUREY_FILENAME, survey);
        messageHelper.setTo(mail.getTo());
        messageHelper.setFrom(mail.getMailServer());
        messageHelper.setSubject(subject);
        messageHelper.setText(mailBody, true); // set true if you using html file as template
        mailSender.send(message);
    }

    public void sendNotificationMail(MailDto mail, String custName) throws MessagingException, IOException {
        MimeMessage message = mailSender.createMimeMessage();
        // set true if you want to include an attachment or using in line image
        MimeMessageHelper messageHelper = new MimeMessageHelper(message, true, "UTF-8");
        // Get variable for mail template
        Context context = new Context();
        context.setVariable("name", mail.getReciverName());
        context.setVariable("custName", custName);
        // Get mail template under resource folder
        String mailBody = templateEngine.process(WebGuiConstant.MAIL_NOTIF_TEMPLATE_PATH, context);
        // Set mail header and body
        String subject = mail.getSubject().replaceAll("\\bname\\b", custName);
        messageHelper.setTo(mail.getTo());
        messageHelper.setFrom(mail.getMailServer());
        messageHelper.setSubject(subject);
        messageHelper.setText(mailBody, true); // set true if you using html file as template
        mailSender.send(message);
    }

    public void sendNotificationRejectMail(RejectDto mail, boolean isBM, boolean isRejected) throws MessagingException, IOException {
        MimeMessage message = mailSender.createMimeMessage();
        String mailBody;
        // set true if you want to include an attachment or using in line image
        MimeMessageHelper messageHelper = new MimeMessageHelper(message, true, "UTF-8");
        // Get variable for mail template
        Context context = new Context();
        context.setVariable("custName", mail.getCustName());
        context.setVariable("appidNo", mail.getAppidNo());
        context.setVariable("sentra", mail.getSentra());
        context.setVariable("alasan", mail.getAlasan());
        context.setVariable("isRejected", isRejected);
        if (isBM) {
            context.setVariable("mms", mail.getMms());
            mailBody = templateEngine.process(WebGuiConstant.MAIL_NOTIF_REJECT_SCORING_BM_TEMPLATE_PATH, context);
        } else {
            mailBody = templateEngine.process(WebGuiConstant.MAIL_NOTIF_REJECT_SCORING_TEMPLATE_PATH, context);
        }
        // Set mail header and body
        String subject = mail.getSubject().replaceAll("\\bname\\b", mail.getCustName());
        messageHelper.setTo(mail.getTo());
        messageHelper.setFrom(mail.getMailServer());
        messageHelper.setSubject(subject);
        messageHelper.setText(mailBody, true); // set true if you using html file as template
        mailSender.send(message);
    }

    public void sendNotificationReturnMail(MailDto mail, String mailFrom, String namaNasabah, String alasan) throws MessagingException, IOException {
        MimeMessage message = mailSender.createMimeMessage();
        // set true if you want to include an attachment or using in line image
        MimeMessageHelper messageHelper = new MimeMessageHelper(message, true, "UTF-8");
        // Get variable for mail template
        Context context = new Context();
        context.setVariable("alasan", alasan);
        context.setVariable("namaNasabah", namaNasabah);
        context.setVariable("fromAddress", mailFrom);
        context.setVariable("username", mail.getReciverName());
        // Get mail template under resource folder
        String mailBody = templateEngine.process(WebGuiConstant.MAIL_NOTIF_RETURN_TEMPLATE_PATH, context);
        // Set mail header and body
        String subject = mail.getSubject().replaceAll("\\bname\\b", namaNasabah);
        messageHelper.setTo(mail.getTo());
        messageHelper.setFrom(mail.getMailServer());
        messageHelper.setSubject(subject);
        messageHelper.setText(mailBody, true); // set true if you using html file as template
        mailSender.send(message);
    }

    private Map<String, Object> castObjectToMap(Object object) {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.convertValue(object, new TypeReference<Map<String, Object>>() {
        });
    }

    private String getDeviationMessage(String swid, String userId, boolean isInReview) {
        if (isInReview) {
            if (checkIsBc(userId)) {
                String status = AP3RSvc.checkDeviasi(Long.parseLong(swid));
                boolean isDevExist = status != null ? true : false;
                if (isDevExist) {
                    return "Deviasi sudah di buat";
                } else {
                    return "Deviasi belum di buat";
                }
            } else {
                return "";
            }
        } else {
            return "";
        }
    }

    private boolean checkIsBc(String userId) {
        User user = usrSvc.findUserByUsername(userId);
        if ("3".equalsIgnoreCase(user.getRoleUser())) {
            return true;
        } else {
            return false;
        }
    }

}