package id.co.telkomsigma.btpns.mprospera.manager.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import id.co.telkomsigma.btpns.mprospera.dao.SwSurveyPhotoDao;
import id.co.telkomsigma.btpns.mprospera.manager.SwSurveyPhotoManager;
import id.co.telkomsigma.btpns.mprospera.model.sw.SwSurveyPhoto;

@Service("swSurveyPhotoManager")
public class SwSurveyPhotoManagerImpl implements SwSurveyPhotoManager {

    @Autowired
    private SwSurveyPhotoDao swSurveyPhotoDao;

    @Override
    public SwSurveyPhoto getSwSurveyPhoto(Long swid) {
        return swSurveyPhotoDao.getSwSurveyPhoto(swid);
    }

}