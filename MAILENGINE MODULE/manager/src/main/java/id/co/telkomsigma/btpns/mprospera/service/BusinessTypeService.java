package id.co.telkomsigma.btpns.mprospera.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.telkomsigma.btpns.mprospera.manager.BusinessTypeManager;
import id.co.telkomsigma.btpns.mprospera.model.sw.BusinessType;

@Service("businessTypeService")
public class BusinessTypeService {

	@Autowired
	private BusinessTypeManager businessTypeManager;
	
	public BusinessType findByBusinessId(Long id) {
		return businessTypeManager.findByBusinessId(id);
	}
	
	public List<BusinessType> findAll(){
		return businessTypeManager.findAll();
	}
	
	public List<Long> findAllBusinessTypeId() {
		return businessTypeManager.findAllBusinessTypeId();
	}
}

