package id.co.telkomsigma.btpns.mprospera.dao;

import id.co.telkomsigma.btpns.mprospera.model.sda.AreaProvinceDetails;

import org.springframework.data.jpa.repository.JpaRepository;

public interface AreaProvinceDetailsDao extends JpaRepository<AreaProvinceDetails, Long> {

    Integer countByAreaDetailId(long parseLong);

}