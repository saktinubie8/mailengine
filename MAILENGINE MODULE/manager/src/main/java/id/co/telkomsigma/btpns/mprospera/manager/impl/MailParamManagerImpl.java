package id.co.telkomsigma.btpns.mprospera.manager.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.telkomsigma.btpns.mprospera.dao.MailParamDao;
import id.co.telkomsigma.btpns.mprospera.manager.MailParamManager;
import id.co.telkomsigma.btpns.mprospera.model.parameter.MailParameter;

@Service("mailParamManager")
public class MailParamManagerImpl implements MailParamManager {
	
	@Autowired
	private MailParamDao mailParamDao;

	@Override
	public List<MailParameter> getAll() {
		return mailParamDao.findAll();
	}

	@Override
	public MailParameter getParamById(Long id) {
		return mailParamDao.findById(id);
	}

	@Override
	public MailParameter getParamByName(String messageParam) {
		return mailParamDao.findByMessageParam(messageParam);
	}

	@Override
	public void clearCache() {

	}

}
