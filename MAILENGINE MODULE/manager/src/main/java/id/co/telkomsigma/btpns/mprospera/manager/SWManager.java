package id.co.telkomsigma.btpns.mprospera.manager;

import id.co.telkomsigma.btpns.mprospera.model.sw.*;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

public interface SWManager {

    int countAll();

    int countAllByStatus();

    int countAllProduct();

    int countAllBusinessType();

    Page<SurveyWawancara> findAll(List<String> kelIdList);

    Page<LoanProduct> findAllProduct();

    Page<LoanProduct> findAllProductPageable(PageRequest pageRequest);

    Page<BusinessType> findAllBusinessType();

    Page<BusinessType> findAllBusinessTypePageable(PageRequest pageRequest);

    Page<BusinessTypeAP3R> findAllBusinessTypeAP3R();

    Page<BusinessTypeAP3R> findAllBusinessTypeAP3RPageable(PageRequest pageRequest);

    Page<SurveyWawancara> findAllNonMs();

    Page<SurveyWawancara> findAllNonMsPageable(PageRequest pageRequest);

    Page<SurveyWawancara> findAllNonMsWithUserId(Long userId);

    Page<SurveyWawancara> findAllNonMsWithUserIdPageable(Long userId, PageRequest pageRequest);

    Page<SurveyWawancara> findByCreatedDateNonMs(Date startDate, Date endDate);

    Page<SurveyWawancara> findByCreatedDateNonMsPageable(Date startDate, Date endDate,
                                                         PageRequest pageRequest);

    Page<SurveyWawancara> findAllByUser(List<String> userIdList);

    Page<SurveyWawancara> findAllByUserPageable(List<String> userIdList, PageRequest pageRequest);

    Page<SurveyWawancara> findByCreatedDateAndUser(List<String> userIdList, Date startDate, Date endDate);

    Page<SurveyWawancara> findByCreatedDateAndUserPageable(List<String> userIdList, Date startDate, Date endDate,
                                                           PageRequest pageRequest);

    Page<SurveyWawancara> findAllByUserMS(List<String> userIdList);

    Page<SurveyWawancara> findAllByUserMSPageable(List<String> userIdList, PageRequest pageRequest);

    Page<SurveyWawancara> findByCreatedDateAndUserMS(List<String> userIdList, Date startDate, Date endDate);

    Page<SurveyWawancara> findByCreatedDateAndUserMSPageable(List<String> userIdList, Date startDate, Date endDate,
                                                             PageRequest pageRequest);

    Page<SurveyWawancara> findByCreatedDatePageable(List<String> kelIdList, Date startDate, Date endDate,
                                                    PageRequest pageRequest);

    Page<SurveyWawancara> findByCreatedDate(List<String> kelIdList, Date startDate, Date endDate);

    Boolean isValidSw(String swId);

    void save(SurveyWawancara surveyWawancara);

    void save(SwIdPhoto swIdPhoto);

    void save(SwSurveyPhoto swSurveyPhoto);

    SwIdPhoto getSwIdPhoto(String swId);

    SwSurveyPhoto getSwSurveyPhoto(String swId);

    SurveyWawancara getSWById(String swId);

    SurveyWawancara findByRrn(String rrn);

    SurveyWawancara findByLocalId(String localId);

    List<ProductPlafond> findByProductId(LoanProduct productId);

    LoanProduct findByProductId(String productId);

    List<SWProductMapping> findProductMapBySwId(Long swId);

    SWProductMapping findProductMapById(Long id);

    List<DirectBuyThings> findProductBySwId(SurveyWawancara sw);

    List<AWGMBuyThings> findAwgmProductBySwId(SurveyWawancara sw);

    SWProductMapping findProductMapByLocalId(String localId);

    List<NeighborRecommendation> findNeighborBySwId(SurveyWawancara sw);

    List<OtherItemCalculation> findOtherItemCalcBySwId(Long swId);

    SWUserBWMPMapping findBySwAndUser(Long sw, Long userId);

    SWUserBWMPMapping findBySwAndLevel(Long sw, String level);

    List<SWUserBWMPMapping> findBySw(Long sw);

    void save(DirectBuyThings directBuyProduct);

    void save(AWGMBuyThings awgmBuyProduct);

    void save(NeighborRecommendation neighbor);

    void save(OtherItemCalculation otherItemCalculation);

    void save(SWProductMapping map);

    void save(SWUserBWMPMapping map);

    void deleteDirectBuy(DirectBuyThings id);

    void deleteAwgmBuy(AWGMBuyThings id);

    void deleteReffNeighbour(NeighborRecommendation id);

    void deleteOtherItemCalc(OtherItemCalculation id);

    void clearCache();

    List<SurveyWawancara> findIsDeletedSwList();
    
    List<SurveyWawancara> findSwByCustomerId(Long customerId, String status);

}