package id.co.telkomsigma.btpns.mprospera.manager;

import id.co.telkomsigma.btpns.mprospera.model.manageApk.ManageApk;

import java.util.List;


public interface ManageApkManager {

    List<ManageApk> getAll();

    void clearCache();

}