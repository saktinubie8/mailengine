package id.co.telkomsigma.btpns.mprospera.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import id.co.telkomsigma.btpns.mprospera.model.parameter.MailParameter;

public interface MailParamDao extends JpaRepository<MailParameter, String> {
	
	MailParameter findById(Long id);
	
	MailParameter findByMessageParam(String messageParam);
}
