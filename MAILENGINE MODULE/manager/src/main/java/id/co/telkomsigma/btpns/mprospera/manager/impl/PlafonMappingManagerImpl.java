package id.co.telkomsigma.btpns.mprospera.manager.impl;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Service;
import id.co.telkomsigma.btpns.mprospera.manager.PlafonMappingManager;
import id.co.telkomsigma.btpns.mprospera.pojo.PlafonMapping;

@Service("plafonMappingManager")
public class PlafonMappingManagerImpl implements PlafonMappingManager {

    @PersistenceContext
    EntityManager em;

    String plafonMappingQuery = "select top 1 b.id,b.tenor,b.plafon,b.monthly_installment from t_plafon_mapping as b where b.tenor = :tenor and b.monthly_installment <= :monthlyinstallment order by b.monthly_installment desc";

    @Override
    public List<PlafonMapping> findByMonthlyInstallment(Integer installement, Integer tenor) {
        Query query = this.em.createNativeQuery(plafonMappingQuery, "PlafonMapping");
        query.setParameter("monthlyinstallment", installement);
        query.setParameter("tenor", tenor);
        List<PlafonMapping> platform = query.getResultList();
        em.close();
        return platform;
    }

}