package id.co.telkomsigma.btpns.mprospera.manager;

import java.util.List;

import id.co.telkomsigma.btpns.mprospera.pojo.FinancingHistory;
import id.co.telkomsigma.btpns.mprospera.pojo.LimitTerpakai;
import id.co.telkomsigma.btpns.mprospera.pojo.LoanPrs;
import id.co.telkomsigma.btpns.mprospera.pojo.MappingScoring;
import id.co.telkomsigma.btpns.mprospera.pojo.PlafonHistory;
import id.co.telkomsigma.btpns.mprospera.pojo.SwPreviousIncome;
import id.co.telkomsigma.btpns.mprospera.pojo.TotalAngsuran;

public interface FinancingHistoryManager {

    List<MappingScoring> getMappingScoring(String appId);

    List<FinancingHistory> getFinancingHistory(Long customerId);

    List<FinancingHistory> getTopupHistory(Long customerId);

    List<FinancingHistory> getKonsumtifHistory(Long customerId);

    List<TotalAngsuran> getTotalAngsuran(Long customerId);

    List<LimitTerpakai> getLimitTerpakai(Long customerId);

    List<LoanPrs> getLoanPrs(Long loanId);

    List<SwPreviousIncome> getPreviousIncome(Long customerId);

    List<PlafonHistory> getPlafonHistory(Long customerId);

}