package id.co.telkomsigma.btpns.mprospera.dto;

public class RejectDto extends MailDto{
	
	private String custName;
	private String mms;
	private String appidNo;
	private String sentra;
	private String alasan;

	
	public RejectDto() {
		// TODO Auto-generated constructor stub
	}


	public String getCustName() {
		return custName;
	}

	public void setCustName(String custName) {
		this.custName = custName;
	}

	public String getMms() {
		return mms;
	}

	public void setMms(String mms) {
		this.mms = mms;
	}

	public String getAppidNo() {
		return appidNo;
	}

	public void setAppidNo(String appidNo) {
		this.appidNo = appidNo;
	}

	public String getSentra() {
		return sentra;
	}

	public void setSentra(String sentra) {
		this.sentra = sentra;
	}

	public String getAlasan() {
		return alasan;
	}

	public void setAlasan(String alasan) {
		this.alasan = alasan;
	}
	
	

}
