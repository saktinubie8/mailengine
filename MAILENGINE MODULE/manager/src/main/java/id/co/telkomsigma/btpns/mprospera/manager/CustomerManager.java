package id.co.telkomsigma.btpns.mprospera.manager;

import java.math.BigDecimal;

import id.co.telkomsigma.btpns.mprospera.model.customer.Customer;

public interface CustomerManager {

    Integer countCustomerByUsername(String username);

    void clearCache();

    void save(Customer customer);

    Customer getBySwId(Long swId);

    Customer getByPdkId(Long pdkId);

    Customer findById(long parseLong);

    void delete(Customer customer);
    
    BigDecimal findActiveInstallmentCost(Long custId);

}