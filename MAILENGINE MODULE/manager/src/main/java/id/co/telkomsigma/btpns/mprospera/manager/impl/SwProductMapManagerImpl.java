package id.co.telkomsigma.btpns.mprospera.manager.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.telkomsigma.btpns.mprospera.dao.SwProductMapDao;
import id.co.telkomsigma.btpns.mprospera.manager.SwProductMapManager;
import id.co.telkomsigma.btpns.mprospera.model.sw.SWProductMapping;

@Service("swProductMapManager")
public class SwProductMapManagerImpl implements SwProductMapManager {

	@Autowired
	private SwProductMapDao swProductMapDao;
	
	@Override
	public SWProductMapping findOneSwProductMapBySwId(Long id) {
		return swProductMapDao.findOneSwProductMapBySwId(id);
	}

}
