package id.co.telkomsigma.btpns.mprospera.manager;

import id.co.telkomsigma.btpns.mprospera.model.sw.SwIdPhoto;

public interface SwIdPhotoManager {

    SwIdPhoto getSwIdPhoto(Long swid);

}