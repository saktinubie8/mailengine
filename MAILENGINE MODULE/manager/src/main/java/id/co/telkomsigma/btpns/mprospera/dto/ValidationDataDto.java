package id.co.telkomsigma.btpns.mprospera.dto;

public class ValidationDataDto {
	
	/*
	 * Header
	 */
	private String namaSco;
	private String namaNasabah;
	private String namaMms;
	private String kodeMms;
	private String tahunKe;
	private String bulanKe;
	
	/*
	 * Pengajuan Pembiayaan
	 */
	private String produk;
	private String plafon;
	private String tenor;
	private String angsuran;
	private String angsuranTotal;
	private String margin;
	private String tglCair;
	
	/*
	 * Limit Pembiayaan
	 */
	private String limitPembiayaanTotal;
	private String limitTerpakai;
	private String limitTersisa;
	
	/*
	 * Rasio
	 */
	private String totalPlafonAktif;
	private String totalSaldoTabungan;
	
	/*
	 * Riwayat Pembiayaan - Pembiayaan Utama
	 */

	private RiwayatPembiayaanDto biayaSblmAkhir;
	private RiwayatPembiayaanDto biayaAkhir;
	private RiwayatPembiayaanDto biayaSebelumY;
	
	/*
	 * Riwayat Pembiayaan - Pembiayaan Tambahan
	 */
	
	private RiwayatPembiayaanDto biayaTopup;
	private RiwayatPembiayaanDto biayaKonsumtif;

	
	/*
	 * Riwayat Tabungan
	 */
	private SaldoTabunganDto saldoTabungan;
	private FrekwensiTransaksiDto frek;
	
	/*
	 * Profil Usaha
	 */
	private String jenisUsaha;
	private String deskripsiUsaha;
	private String waktuOperasional;
	private String statusTempatUsaha;
	private String lamaUsaha;
	
	/*
	 * Perhutungan Pendapatan
	 */
	private String pendapatanPenjualan;
	private String pembelian;
	private String pengeluaranUsaha;
	private String pendapatanBersih;
	private String pendapatanLainnya;
	private String pengeluaranNonUsaha;
	private String sisaPenghasilan;
	private String maxAngsuran;
	private String maxAngsuranPersen;
	private String iir;
	
	
	/*
	 * Pendapatan Sebelumnya
	 */
	private boolean incomeDataIsSame;
	private String kenaikanPendapatan;
	private String penurunanPendapatan;
	
	/*
	 * Miscellaneous
	 */
	private String catatan;
	private String kategoriNasabah;
	private String approvalUsername;
	private boolean isInReview;
	private String kodeDeviasi;
	private String status;
	
	public String getNamaSco() {
		return namaSco;
	}
	public void setNamaSco(String namaSco) {
		this.namaSco = namaSco;
	}
	public String getNamaNasabah() {
		return namaNasabah;
	}
	public void setNamaNasabah(String namaNasabah) {
		this.namaNasabah = namaNasabah;
	}
	public String getNamaMms() {
		return namaMms;
	}
	public void setNamaMms(String namaMms) {
		this.namaMms = namaMms;
	}
	public String getTahunKe() {
		return tahunKe;
	}
	public void setTahunKe(String tahunKe) {
		this.tahunKe = tahunKe;
	}
	public String getProduk() {
		return produk;
	}
	public void setProduk(String produk) {
		this.produk = produk;
	}
	public String getPlafon() {
		return plafon;
	}
	public void setPlafon(String plafon) {
		this.plafon = plafon;
	}
	public String getTenor() {
		return tenor;
	}
	public void setTenor(String tenor) {
		this.tenor = tenor;
	}
	public String getAngsuran() {
		return angsuran;
	}
	public void setAngsuran(String angsuran) {
		this.angsuran = angsuran;
	}
	public String getAngsuranTotal() {
		return angsuranTotal;
	}
	public void setAngsuranTotal(String angsuranTotal) {
		this.angsuranTotal = angsuranTotal;
	}
	public String getMargin() {
		return margin;
	}
	public void setMargin(String margin) {
		this.margin = margin;
	}
	public String getLimitPembiayaanTotal() {
		return limitPembiayaanTotal;
	}
	public void setLimitPembiayaanTotal(String limitPembiayaanTotal) {
		this.limitPembiayaanTotal = limitPembiayaanTotal;
	}
	public String getLimitTerpakai() {
		return limitTerpakai;
	}
	public void setLimitTerpakai(String limitTerpakai) {
		this.limitTerpakai = limitTerpakai;
	}
	public String getLimitTersisa() {
		return limitTersisa;
	}
	public void setLimitTersisa(String limitTersisa) {
		this.limitTersisa = limitTersisa;
	}
	public String getTotalPlafonAktif() {
		return totalPlafonAktif;
	}
	public void setTotalPlafonAktif(String totalPlafonAktif) {
		this.totalPlafonAktif = totalPlafonAktif;
	}
	public String getTotalSaldoTabungan() {
		return totalSaldoTabungan;
	}
	public void setTotalSaldoTabungan(String totalSaldoTabungan) {
		this.totalSaldoTabungan = totalSaldoTabungan;
	}
	public RiwayatPembiayaanDto getBiayaSblmAkhir() {
		return biayaSblmAkhir;
	}
	public void setBiayaSblmAkhir(RiwayatPembiayaanDto biayaSblmAkhir) {
		this.biayaSblmAkhir = biayaSblmAkhir;
	}
	public RiwayatPembiayaanDto getBiayaAkhir() {
		return biayaAkhir;
	}
	public void setBiayaAkhir(RiwayatPembiayaanDto biayaAkhir) {
		this.biayaAkhir = biayaAkhir;
	}
	public RiwayatPembiayaanDto getBiayaSebelumY() {
		return biayaSebelumY;
	}
	public void setBiayaSebelumY(RiwayatPembiayaanDto biayaSebelumY) {
		this.biayaSebelumY = biayaSebelumY;
	}
	public RiwayatPembiayaanDto getBiayaKonsumtif() {
		return biayaKonsumtif;
	}
	public void setBiayaKonsumtif(RiwayatPembiayaanDto biayaKonsumtif) {
		this.biayaKonsumtif = biayaKonsumtif;
	}
	public SaldoTabunganDto getSaldoTabungan() {
		return saldoTabungan;
	}
	public void setSaldoTabungan(SaldoTabunganDto saldoTabungan) {
		this.saldoTabungan = saldoTabungan;
	}
	public FrekwensiTransaksiDto getFrek() {
		return frek;
	}
	public void setFrek(FrekwensiTransaksiDto frek) {
		this.frek = frek;
	}
	public String getJenisUsaha() {
		return jenisUsaha;
	}
	public void setJenisUsaha(String jenisUsaha) {
		this.jenisUsaha = jenisUsaha;
	}
	public String getDeskripsiUsaha() {
		return deskripsiUsaha;
	}
	public void setDeskripsiUsaha(String deskripsiUsaha) {
		this.deskripsiUsaha = deskripsiUsaha;
	}
	public String getWaktuOperasional() {
		return waktuOperasional;
	}
	public void setWaktuOperasional(String waktuOperasional) {
		this.waktuOperasional = waktuOperasional;
	}
	public String getStatusTempatUsaha() {
		return statusTempatUsaha;
	}
	public void setStatusTempatUsaha(String statusTempatUsaha) {
		this.statusTempatUsaha = statusTempatUsaha;
	}
	public String getLamaUsaha() {
		return lamaUsaha;
	}
	public void setLamaUsaha(String lamaUsaha) {
		this.lamaUsaha = lamaUsaha;
	}
	public String getPendapatanPenjualan() {
		return pendapatanPenjualan;
	}
	public void setPendapatanPenjualan(String pendapatanPenjualan) {
		this.pendapatanPenjualan = pendapatanPenjualan;
	}
	public String getPembelian() {
		return pembelian;
	}
	public void setPembelian(String pembelian) {
		this.pembelian = pembelian;
	}
	public String getPengeluaranUsaha() {
		return pengeluaranUsaha;
	}
	public void setPengeluaranUsaha(String pengeluaranUsaha) {
		this.pengeluaranUsaha = pengeluaranUsaha;
	}
	public String getPendapatanBersih() {
		return pendapatanBersih;
	}
	public void setPendapatanBersih(String pendapatanBersih) {
		this.pendapatanBersih = pendapatanBersih;
	}
	public String getPendapatanLainnya() {
		return pendapatanLainnya;
	}
	public void setPendapatanLainnya(String pendapatanLainnya) {
		this.pendapatanLainnya = pendapatanLainnya;
	}

	
	public String getSisaPenghasilan() {
		return sisaPenghasilan;
	}
	public void setSisaPenghasilan(String sisaPenghasilan) {
		this.sisaPenghasilan = sisaPenghasilan;
	}
	public boolean isIncomeDataIsSame() {
		return incomeDataIsSame;
	}
	public void setIncomeDataIsSame(boolean incomeDataIsSame) {
		this.incomeDataIsSame = incomeDataIsSame;
	}
	public String getKenaikanPendapatan() {
		return kenaikanPendapatan;
	}
	public void setKenaikanPendapatan(String kenaikanPendapatan) {
		this.kenaikanPendapatan = kenaikanPendapatan;
	}
	public String getPenurunanPendapatan() {
		return penurunanPendapatan;
	}
	public void setPenurunanPendapatan(String penurunanPendapatan) {
		this.penurunanPendapatan = penurunanPendapatan;
	}
	public String getCatatan() {
		return catatan;
	}
	public void setCatatan(String catatan) {
		this.catatan = catatan;
	}
	public String getKategoriNasabah() {
		return kategoriNasabah;
	}
	public void setKategoriNasabah(String kategoriNasabah) {
		this.kategoriNasabah = kategoriNasabah;
	}
	public RiwayatPembiayaanDto getBiayaTopup() {
		return biayaTopup;
	}
	public void setBiayaTopup(RiwayatPembiayaanDto biayaTopup) {
		this.biayaTopup = biayaTopup;
	}
	public String getIir() {
		return iir;
	}
	public void setIir(String iir) {
		this.iir = iir;
	}
	public String getKodeMms() {
		return kodeMms;
	}
	public void setKodeMms(String kodeMms) {
		this.kodeMms = kodeMms;
	}
	public String getTglCair() {
		return tglCair;
	}
	public void setTglCair(String tglCair) {
		this.tglCair = tglCair;
	}
	public String getPengeluaranNonUsaha() {
		return pengeluaranNonUsaha;
	}
	public void setPengeluaranNonUsaha(String pengeluaranNonUsaha) {
		this.pengeluaranNonUsaha = pengeluaranNonUsaha;
	}
	public String getMaxAngsuran() {
		return maxAngsuran;
	}
	public void setMaxAngsuran(String maxAngsuran) {
		this.maxAngsuran = maxAngsuran;
	}
	public String getMaxAngsuranPersen() {
		return maxAngsuranPersen;
	}
	public void setMaxAngsuranPersen(String maxAngsuranPersen) {
		this.maxAngsuranPersen = maxAngsuranPersen;
	}
	public String getApprovalUsername() {
		return approvalUsername;
	}
	public void setApprovalUsername(String approvalUsername) {
		this.approvalUsername = approvalUsername;
	}
	public String getBulanKe() {
		return bulanKe;
	}
	public void setBulanKe(String bulanKe) {
		this.bulanKe = bulanKe;
	}
	public boolean getIsInReview() {
		return isInReview;
	}
	public void setIsInReview(boolean isInReview) {
		this.isInReview = isInReview;
	}
	public String getKodeDeviasi() {
		return kodeDeviasi;
	}
	public void setKodeDeviasi(String kodeDeviasi) {
		this.kodeDeviasi = kodeDeviasi;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	
}
