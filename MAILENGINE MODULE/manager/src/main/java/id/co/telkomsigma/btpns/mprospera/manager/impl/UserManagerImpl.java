package id.co.telkomsigma.btpns.mprospera.manager.impl;

import id.co.telkomsigma.btpns.mprospera.dao.UserDao;
import id.co.telkomsigma.btpns.mprospera.manager.UserManager;
import id.co.telkomsigma.btpns.mprospera.model.user.User;
import id.co.telkomsigma.btpns.mprospera.pojo.UserEmail;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 * Created by daniel on 3/31/15.
 */
@Service("userManager")
public class UserManagerImpl implements UserManager {

    @Autowired
    private UserDao userDao;
    
	@PersistenceContext
	private EntityManager em;
    
    String queryStr = "select id,user_login, full_name, email from m_users where role=3 and account_enabled=1 and limit >= (select plafon FROM t_sw_product_map where sw_id=:swid)";
    
    private static final Logger log = LoggerFactory.getLogger(UserManagerImpl.class);

    @Override
    public User getUserByUsername(String username) {
        return userDao.findByUsername(username.toLowerCase());
    }

    @Override
    public User getUserByUserId(Long userId) {
        User user = userDao.findByUserId(userId);
        return user;
    }

    @Override
    @CacheEvict(value = {"wln.user.userByUsername", "wln.user.userByLocId"}, allEntries = true, beforeInvocation = true)
    public void clearCache() {

    }

    @Override
    @Cacheable(value = "wln.user.userByLocId", unless = "#result == null")
    public List<User> getUserByLocId(String locId) {
        // TODO Auto-generated method stub
        List<User> userWisma = userDao.findByOfficeCode(locId);
        return userWisma;
    }

    @Override
    public List<User> findUserNonMS() {
        String role = "3";
        return userDao.findByRoleUser(role);
    }


	
	@Override
	public List<UserEmail> findUserByLimit(long id){
		Query query = this.em.createNativeQuery(queryStr,"UserEmail");
		query.setParameter("swid", id);
		List<UserEmail> userEmails = (List<UserEmail>) query.getResultList();
		em.close();
		return userEmails;
	}

	@Override
	public User findByEmail(String email) {
		return userDao.findByEmail(email);
	}

	@Override
//    @Cacheable(value = "vsn.user.sessionKeyByUsername", unless = "#result == null")
	public String getSessionKeyByUsername(String username) {
        log.debug("in direct query getSessionKeyByUsername >> " + username);
        return userDao.findSessionKeyByUsername(username.toLowerCase());
	}

}