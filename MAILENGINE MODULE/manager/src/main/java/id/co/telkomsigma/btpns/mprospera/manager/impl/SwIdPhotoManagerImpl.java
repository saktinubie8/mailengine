package id.co.telkomsigma.btpns.mprospera.manager.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import id.co.telkomsigma.btpns.mprospera.dao.SwIdPhotoDao;
import id.co.telkomsigma.btpns.mprospera.manager.SwIdPhotoManager;
import id.co.telkomsigma.btpns.mprospera.model.sw.SwIdPhoto;

@Service("swIdPhotoManager")
public class SwIdPhotoManagerImpl implements SwIdPhotoManager {

    @Autowired
    private SwIdPhotoDao swIdPhotoDao;

    @Override
    public SwIdPhoto getSwIdPhoto(Long swid) {
        return swIdPhotoDao.getSwIdPhoto(swid);
    }

}