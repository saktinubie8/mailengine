
package id.co.telkomsigma.btpns.mprospera.manager;

import java.util.List;

import id.co.telkomsigma.btpns.mprospera.model.sw.BusinessType;

public interface BusinessTypeManager {
	
	BusinessType findByBusinessId(Long id);
	List<BusinessType> findAll();
	List<Long> findAllBusinessTypeId();

}
