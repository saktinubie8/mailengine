package id.co.telkomsigma.btpns.mprospera.manager.impl;

import id.co.telkomsigma.btpns.mprospera.dao.AuditLogDao;
import id.co.telkomsigma.btpns.mprospera.manager.AuditLogManager;
import id.co.telkomsigma.btpns.mprospera.model.security.AuditLog;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("auditLogManager")
public class AuditLogManagerImpl implements AuditLogManager {

    @Autowired
    private AuditLogDao auditLogDao;

    @Override
    public List<AuditLog> getAll() {
        return auditLogDao.findAll();
    }

    @Override
    public AuditLog insertAuditLog(AuditLog auditLog) {
//		auditLog = auditLogDao.save(auditLog);
        return null;
    }

}