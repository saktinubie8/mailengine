package id.co.telkomsigma.btpns.mprospera.manager;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import id.co.telkomsigma.btpns.mprospera.model.sw.AP3R;
import id.co.telkomsigma.btpns.mprospera.model.sw.FundedThings;

public interface AP3RManager {

    int countAll();

    void save(FundedThings product);

    void delete(FundedThings products);

    void save(AP3R ap3r);

    List<FundedThings> findByAp3rId(AP3R ap3r);

    AP3R findByAp3rId(Long id);

    AP3R findByLocalId(String localId);

    Page<AP3R> findAllByUser(List<String> userIdList);

    Page<AP3R> findAllByUserPageable(List<String> userIdList, PageRequest pageRequest);

    Page<AP3R> findByCreatedDateAndUser(List<String> userIdList, Date startDate, Date endDate);

    Page<AP3R> findByCreatedDateAndUserPageable(List<String> userIdList, Date startDate, Date endDate,
                                                PageRequest pageRequest);

    Page<AP3R> findAllByUserMS(List<String> userIdList);

    Page<AP3R> findAllByUserMSPageable(List<String> userIdList, PageRequest pageRequest);

    Page<AP3R> findByCreatedDateAndUserMS(List<String> userIdList, Date startDate, Date endDate);

    Page<AP3R> findByCreatedDateAndUserMSPageable(List<String> userIdList, Date startDate, Date endDate,
                                                  PageRequest pageRequest);

    Page<AP3R> findForDeviation();

    Page<AP3R> findForDeviationPageable(PageRequest pageRequest);

    Page<AP3R> findForDeviationByCreatedDate(Date startDate, Date endDate);

    Page<AP3R> findForDeviationByCreatedDatePageable(Date startDate, Date endDate, PageRequest pageRequest);

    List<AP3R> findIsDeletedAp3rList();

    List<AP3R> findAp3rListBySwId(Long swId);
    
    String checkDeviasi(long swid);

}