package id.co.telkomsigma.btpns.mprospera.dto;

import java.math.BigDecimal;

public class ApprovalVariable {
	
	private BigDecimal outcome;
	private BigDecimal pendapatanBersih;
	private BigDecimal angsuran;
	private BigDecimal maxAngsuran;
	private BigDecimal maxAngsuranPercent;
	
	public ApprovalVariable() {
		
	}
	
	public ApprovalVariable(BigDecimal outcome,BigDecimal pendapatanBersih, BigDecimal angsuran, BigDecimal maxAngsuran,BigDecimal maxAngsuranPercent) {
		this.outcome = outcome;
		this.pendapatanBersih = pendapatanBersih;
		this.angsuran = angsuran;
		this.maxAngsuran = maxAngsuran;
		this.maxAngsuranPercent = maxAngsuranPercent;
	}
	
	
	public BigDecimal getOutcome() {
		return outcome;
	}
	public void setOutcome(BigDecimal outcome) {
		this.outcome = outcome;
	}
	public BigDecimal getPendapatanBersih() {
		return pendapatanBersih;
	}
	public void setPendapatanBersih(BigDecimal pendapatanBersih) {
		this.pendapatanBersih = pendapatanBersih;
	}
	public BigDecimal getAngsuran() {
		return angsuran;
	}
	public void setAngsuran(BigDecimal angsuran) {
		this.angsuran = angsuran;
	}
	public BigDecimal getMaxAngsuran() {
		return maxAngsuran;
	}
	public void setMaxAngsuran(BigDecimal maxAngsuran) {
		this.maxAngsuran = maxAngsuran;
	}
	public BigDecimal getMaxAngsuranPercent() {
		return maxAngsuranPercent;
	}
	public void setMaxAngsuranPercent(BigDecimal maxAngsuranPercent) {
		this.maxAngsuranPercent = maxAngsuranPercent;
	}
	
	

}
