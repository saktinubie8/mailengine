package id.co.telkomsigma.btpns.mprospera.manager.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.telkomsigma.btpns.mprospera.dao.LoanProductDao;
import id.co.telkomsigma.btpns.mprospera.manager.LoanProductManager;
import id.co.telkomsigma.btpns.mprospera.model.sw.LoanProduct;

@Service("loanProductManager")
public class LoanProductManagerImpl implements LoanProductManager {

	@Autowired
	private LoanProductDao loanProductDao;
	@Override
	public LoanProduct findByProductId(Long id) {
		// TODO Auto-generated method stub
		return loanProductDao.findByProductId(id);
	}

}
