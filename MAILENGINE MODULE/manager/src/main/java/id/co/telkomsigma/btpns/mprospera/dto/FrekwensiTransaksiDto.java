package id.co.telkomsigma.btpns.mprospera.dto;

public class FrekwensiTransaksiDto {
	
	private String setorSaatIni;
	private String tarikSaatIni;
	private String setorBulanLalu;
	private String tarikBulanLalu;
	private String setorDuaBulanLalu;
	private String tarikDuaBulanLalu;
	private String setorTigaBulanLalu;
	private String tarikTigaBulanLalu;
	
	public FrekwensiTransaksiDto() {
	}
	
	public FrekwensiTransaksiDto(String setorSaatIni, String tarikSaatIni, String setorBulanLalu, String tarikBulanLalu, String setorDuaBulanLalu, String tarikDuaBulanLalu, String setorTigaBulanLalu, String tarikTigaBulanLalu) {
		this.setorSaatIni = setorSaatIni;
		this.tarikSaatIni = tarikSaatIni;
		this.setorBulanLalu = setorBulanLalu;
		this.tarikBulanLalu = tarikBulanLalu;
		this.setorDuaBulanLalu = setorDuaBulanLalu;
		this.tarikDuaBulanLalu = tarikDuaBulanLalu;
		this.setorTigaBulanLalu = setorTigaBulanLalu;
		this.tarikTigaBulanLalu = tarikTigaBulanLalu;
	}
	
	public String getSetorSaatIni() {
		return setorSaatIni;
	}
	public void setSetorSaatIni(String setorSaatIni) {
		this.setorSaatIni = setorSaatIni;
	}
	public String getTarikSaatIni() {
		return tarikSaatIni;
	}
	public void setTarikSaatIni(String tarikSaatIni) {
		this.tarikSaatIni = tarikSaatIni;
	}
	public String getSetorBulanLalu() {
		return setorBulanLalu;
	}
	public void setSetorBulanLalu(String setorBulanLalu) {
		this.setorBulanLalu = setorBulanLalu;
	}
	public String getTarikBulanLalu() {
		return tarikBulanLalu;
	}
	public void setTarikBulanLalu(String tarikBulanLalu) {
		this.tarikBulanLalu = tarikBulanLalu;
	}
	public String getSetorDuaBulanLalu() {
		return setorDuaBulanLalu;
	}
	public void setSetorDuaBulanLalu(String setorDuaBulanLalu) {
		this.setorDuaBulanLalu = setorDuaBulanLalu;
	}
	public String getTarikDuaBulanLalu() {
		return tarikDuaBulanLalu;
	}
	public void setTarikDuaBulanLalu(String tarikDuaBulanLalu) {
		this.tarikDuaBulanLalu = tarikDuaBulanLalu;
	}
	public String getSetorTigaBulanLalu() {
		return setorTigaBulanLalu;
	}
	public void setSetorTigaBulanLalu(String setorTigaBulanLalu) {
		this.setorTigaBulanLalu = setorTigaBulanLalu;
	}
	public String getTarikTigaBulanLalu() {
		return tarikTigaBulanLalu;
	}
	public void setTarikTigaBulanLalu(String tarikTigaBulanLalu) {
		this.tarikTigaBulanLalu = tarikTigaBulanLalu;
	}
	
	

}
