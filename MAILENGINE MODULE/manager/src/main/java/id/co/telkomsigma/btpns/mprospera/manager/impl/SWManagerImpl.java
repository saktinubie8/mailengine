package id.co.telkomsigma.btpns.mprospera.manager.impl;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.dao.*;
import id.co.telkomsigma.btpns.mprospera.model.sw.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import id.co.telkomsigma.btpns.mprospera.manager.SWManager;
import org.springframework.transaction.annotation.Transactional;

@SuppressWarnings("RedundantIfStatement")
@Service("swManager")
public class SWManagerImpl implements SWManager {

    @Autowired
    SWDao swDao;

    @Autowired
    SwIdPhotoDao swIdPhotoDao;

    @Autowired
    SwSurveyPhotoDao swSurveyPhotoDao;

    @Autowired
    SwUserBwmpMapDao swUserBwmpMapDao;

    @Autowired
    private LoanProductDao loanProductDao;

    @Autowired
    private BusinessTypeDao businessTypeDao;

    @Autowired
    private BusinessTypeAP3RDao businessTypeAp3rDao;

    @Autowired
    private ProductPlafondDao productPlafondDao;

    @Autowired
    private DirectBuyDao directBuyDao;

    @Autowired
    private AWGMDao awgmDao;

    @Autowired
    private NeighborDao neighborDao;

    @Autowired
    private SwProductMapDao swProductMapDao;

    @Autowired
    private OtherItemCalculationDao otherItemCalculationDao;

    @PersistenceContext
    public EntityManager em;

    @Override
    @Cacheable(value = "wln.sw.countAllSW", unless = "#result == null")
    public int countAll() {
        // TODO Auto-generated method stub
        return swDao.countAll();
    }

    @Override
    @Cacheable(value = "wln.sw.countAllSWByStatus", unless = "#result == null")
    public int countAllByStatus() {
        return swDao.countAllByStatus(WebGuiConstant.STATUS_WAITING_APPROVAL);
    }

    @Override
    public int countAllProduct() {
        // TODO Auto-generated method stub
        return loanProductDao.countAll();
    }

    @Override
    public int countAllBusinessType() {
        // TODO Auto-generated method stub
        return businessTypeDao.countAll();
    }

    @Override
    public Page<SurveyWawancara> findAll(List<String> kelIdList) {
        // TODO Auto-generated method stub
        return swDao.findByAreaIdInAndLocalIdIsNotNullAndIsDeleted(kelIdList, new PageRequest(0, countAll()), false);
    }

    @Override
    public Page<SurveyWawancara> findByCreatedDate(List<String> kelIdList, Date startDate, Date endDate) {
        // TODO Auto-generated method stub
        Calendar cal = Calendar.getInstance();
        Calendar calStart = Calendar.getInstance();
        cal.setTime(endDate);
        calStart.setTime(startDate);
        cal.add(Calendar.DATE, 1);
        calStart.add(Calendar.DATE, -7);
        return swDao.findByCreatedDate(kelIdList, calStart.getTime(), cal.getTime(), new PageRequest(0, Integer.MAX_VALUE));
    }

    @Override
    public Page<SurveyWawancara> findByCreatedDatePageable(List<String> kelIdList, Date startDate, Date endDate, PageRequest pageRequest) {
        // TODO Auto-generated method stub
        Calendar cal = Calendar.getInstance();
        Calendar calStart = Calendar.getInstance();
        cal.setTime(endDate);
        calStart.setTime(startDate);
        cal.add(Calendar.DATE, 1);
        calStart.add(Calendar.DATE, -7);
        return swDao.findByCreatedDate(kelIdList, calStart.getTime(), cal.getTime(), pageRequest);
    }

    @Override
    public Boolean isValidSw(String swId) {
        Integer count = swDao.countBySwId(Long.parseLong(swId));
        if (count > 0)
            return true;
        else
            return false;
    }

    @Override
    @Caching(evict = {@CacheEvict(value = "wln.sw.countAllSW", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "wln.sw.countAllSWByStatus", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "wln.sw.findByLocalId", key = "#surveyWawancara.localId", beforeInvocation = true),
            @CacheEvict(value = "hwk.sw.countAllSW", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "hwk.sw.findIsDeletedSwList", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "hwk.sw.findByUserMSWithDate", allEntries = true, beforeInvocation = true)})
    public void save(SurveyWawancara surveyWawancara) {
        // TODO Auto-generated method stub
        swDao.save(surveyWawancara);

    }

    @Override
    public void save(SwIdPhoto swIdPhoto) {
        // TODO Auto-generated method stub
        swIdPhotoDao.save(swIdPhoto);

    }

    @Override
    public void save(SwSurveyPhoto SwSurveyPhoto) {
        // TODO Auto-generated method stub
        swSurveyPhotoDao.save(SwSurveyPhoto);

    }

    @Override
    public SwIdPhoto getSwIdPhoto(String swId) {
        // TODO Auto-generated method stub
        return swIdPhotoDao.getSwIdPhoto(Long.parseLong(swId));
    }

    @Override
    public SwSurveyPhoto getSwSurveyPhoto(String swId) {
        // TODO Auto-generated method stub
        return swSurveyPhotoDao.getSwSurveyPhoto(Long.parseLong(swId));
    }

    @Override
    public SurveyWawancara getSWById(String swId) {
        // TODO Auto-generated method stub
        return swDao.findSwBySwId(Long.parseLong(swId));
    }

    @Override
    @CacheEvict(value = {"wln.sw.countAllSW", "wln.sw.countAllSWByStatus", "wln.neighbor.findNeighborBySwId",
            "wln.sw.findSwMapApprovalBySwId", "wln.sw.findByLocalId", "wln.loan.product.findAllProduct"}, allEntries = true, beforeInvocation = true)
    public void clearCache() {
        // TODO Auto-generated method stub

    }

    @Override
    public SurveyWawancara findByRrn(String rrn) {
        // TODO Auto-generated method stub
        return swDao.findByRrnAndLocalIdIsNotNull(rrn);
    }

    @Override
    public Page<SurveyWawancara> findAllByUser(List<String> userIdList) {
        // TODO Auto-generated method stub
        return swDao.findByCreatedByInAndLocalIdIsNotNullAndIsDeleted(userIdList, new PageRequest(0, countAll()), false);
    }

    @Override
    public Page<SurveyWawancara> findAllByUserPageable(List<String> userIdList, PageRequest pageRequest) {
        // TODO Auto-generated method stub
        return swDao.findByCreatedByInAndLocalIdIsNotNullAndIsDeleted(userIdList, pageRequest, false);
    }

    @Override
    public Page<SurveyWawancara> findByCreatedDateAndUser(List<String> userIdList, Date startDate, Date endDate) {
        // TODO Auto-generated method stub
        Calendar cal = Calendar.getInstance();
        Calendar calStart = Calendar.getInstance();
        cal.setTime(endDate);
        calStart.setTime(startDate);
        cal.add(Calendar.DATE, 1);
        calStart.add(Calendar.DATE, -7);
        return swDao.findByCreatedDateAndUser(userIdList, calStart.getTime(), cal.getTime(), new PageRequest(0, Integer.MAX_VALUE));
    }

    @Override
    public Page<SurveyWawancara> findByCreatedDateAndUserPageable(List<String> userIdList, Date startDate,
                                                                  Date endDate, PageRequest pageRequest) {
        // TODO Auto-generated method stub
        Calendar cal = Calendar.getInstance();
        Calendar calStart = Calendar.getInstance();
        cal.setTime(endDate);
        calStart.setTime(startDate);
        cal.add(Calendar.DATE, 1);
        calStart.add(Calendar.DATE, -7);
        return swDao.findByCreatedDateAndUser(userIdList, calStart.getTime(), cal.getTime(), pageRequest);
    }

    @Override
    public Page<SurveyWawancara> findAllByUserMS(List<String> userIdList) {
        return swDao.findByCreatedByInAndLocalIdIsNotNullAndIsDeletedAndStatus(userIdList, new PageRequest(0, countAll()), false, WebGuiConstant.STATUS_WAITING_APPROVAL);
    }

    @Override
    public Page<SurveyWawancara> findAllByUserMSPageable(List<String> userIdList, PageRequest pageRequest) {
        return swDao.findByCreatedByInAndLocalIdIsNotNullAndIsDeletedAndStatus(userIdList, pageRequest, false, WebGuiConstant.STATUS_WAITING_APPROVAL);
    }

    @Override
    public Page<SurveyWawancara> findByCreatedDateAndUserMS(List<String> userIdList, Date startDate, Date endDate) {
        Calendar cal = Calendar.getInstance();
        Calendar calStart = Calendar.getInstance();
        cal.setTime(endDate);
        calStart.setTime(startDate);
        cal.add(Calendar.DATE, 1);
        calStart.add(Calendar.DATE, -7);
        return swDao.findByCreatedDateAndUserAndStatus(userIdList, calStart.getTime(), cal.getTime(), new PageRequest(0, Integer.MAX_VALUE), WebGuiConstant.STATUS_WAITING_APPROVAL);
    }

    @Override
    public Page<SurveyWawancara> findByCreatedDateAndUserMSPageable(List<String> userIdList, Date startDate, Date endDate, PageRequest pageRequest) {
        Calendar cal = Calendar.getInstance();
        Calendar calStart = Calendar.getInstance();
        cal.setTime(endDate);
        calStart.setTime(startDate);
        cal.add(Calendar.DATE, 1);
        calStart.add(Calendar.DATE, -7);
        return swDao.findByCreatedDateAndUserAndStatus(userIdList, calStart.getTime(), cal.getTime(), pageRequest, WebGuiConstant.STATUS_WAITING_APPROVAL);
    }

    @Override
    @Caching(evict = {@CacheEvict(value = "hwk.direct.findProductBySwId", key = "#directBuyProduct.swId", beforeInvocation = true)})
    public void save(DirectBuyThings directBuyProduct) {
        // TODO Auto-generated method stub
        directBuyDao.save(directBuyProduct);
    }

    @Override
    @Caching(evict = {@CacheEvict(value = "hwk.awgm.findAwgmProductBySwId", key = "#awgmBuyProduct.swId", beforeInvocation = true)})
    public void save(AWGMBuyThings awgmBuyProduct) {
        // TODO Auto-generated method stub
        awgmDao.save(awgmBuyProduct);
    }

    @Override
    @Caching(evict = {@CacheEvict(value = "wln.swUserBwmpMap.findSwMapApprovalBySwId", key = "#swUserBWMPMapping.swId", beforeInvocation = true),
            @CacheEvict(value = "hwk.swUserBwmpMap.findSWUserBWMPMappingBySW", allEntries = true, beforeInvocation = true)})
    public void save(SWUserBWMPMapping swUserBWMPMapping) {
        // TODO Auto-generated method stub
        swUserBwmpMapDao.save(swUserBWMPMapping);
    }

    @Override
    @Transactional
    @Caching(evict = {@CacheEvict(value = "hwk.direct.findProductBySwId", key = "#id.swId", beforeInvocation = true)})
    public void deleteDirectBuy(DirectBuyThings id) {
        directBuyDao.delete(id);
    }

    @Override
    @Transactional
    @Caching(evict = {@CacheEvict(value = "hwk.awgm.findAwgmProductBySwId", key = "#id.swId", beforeInvocation = true)})
    public void deleteAwgmBuy(AWGMBuyThings id) {
        awgmDao.delete(id);
    }

    @Override
    @Transactional
    @Caching(evict = {@CacheEvict(value = "wln.neighbor.findNeighborBySwId", key = "#id.swId"),
            @CacheEvict(value = "hwk.neighbor.findNeighborBySwId", key = "#id.swId")
    })
    public void deleteReffNeighbour(NeighborRecommendation id) {
        neighborDao.delete(id);
    }

    @Override
    @Transactional
    @Caching(evict = {@CacheEvict(value = "hwk.other.item.findOtherItemCalcBySwId", key = "#id.swId")
    })
    public void deleteOtherItemCalc(OtherItemCalculation id) {
        otherItemCalculationDao.delete(id);
    }


    @Override
    @Caching(evict = {@CacheEvict(value = "wln.neighbor.findNeighborBySwId", key = "#neighbor.swId", beforeInvocation = true),
            @CacheEvict(value = "hwk.neighbor.findNeighborBySwId", key = "#id.swId")})
    public void save(NeighborRecommendation neighbor) {
        // TODO Auto-generated method stub
        neighborDao.save(neighbor);
    }

    @Override
    @Caching(evict = {@CacheEvict(value = "hwk.other.item.findOtherItemCalcBySwId", key = "#id.swId")
    })
    public void save(OtherItemCalculation otherItemCalculation) {
        otherItemCalculationDao.save(otherItemCalculation);
    }

    @Override
    public List<SWProductMapping> findProductMapBySwId(Long swId) {
        // TODO Auto-generated method stub
        return swProductMapDao.findSwProductMapBySwId(swId);
    }

    @Override
    public SWProductMapping findProductMapById(Long id) {
        return swProductMapDao.findByMappingId(id);
    }

    public void setParameter(Query q, Map<String, SurveyWawancara> parameterList) {
        for (String key : parameterList.keySet()) {
            q.setParameter(key, parameterList.get(key));
        }
    }

    @Override
    public List<DirectBuyThings> findProductBySwId(SurveyWawancara sw) {
        // TODO Auto-generated method stub
        return directBuyDao.findBySwId(sw);
    }

    @Override
    public List<AWGMBuyThings> findAwgmProductBySwId(SurveyWawancara sw) {
        // TODO Auto-generated method stub
        return awgmDao.findBySwId(sw);
    }

    @Override
    @Cacheable(value = "wln.neighbor.findNeighborBySwId", unless = "#result == null")
    public List<NeighborRecommendation> findNeighborBySwId(SurveyWawancara sw) {
        // TODO Auto-generated method stub
        return neighborDao.findBySwId(sw);
    }

    @Override
    public List<OtherItemCalculation> findOtherItemCalcBySwId(Long swId) {
        return otherItemCalculationDao.findBySwId(swId);
    }

    @Override
    public SWUserBWMPMapping findBySwAndUser(Long sw, Long userId) {
        return swUserBwmpMapDao.findBySwIdAndUserId(sw, userId);
    }

    @Override
    public SWUserBWMPMapping findBySwAndLevel(Long sw, String level) {
        return swUserBwmpMapDao.findBySwIdAndLevel(sw, level);
    }

    @Override
    public List<SWUserBWMPMapping> findBySw(Long sw) {
        return swUserBwmpMapDao.findBySwId(sw);
    }

    @Override
    @Caching(evict = {@CacheEvict(value = "hwk.swProductMap.findProductMapBySwId", key = "#map.swId")})
    public void save(SWProductMapping map) {
        // TODO Auto-generated method stub
        swProductMapDao.save(map);
    }

    @Override
    public LoanProduct findByProductId(String productId) {
        // TODO Auto-generated method stub
        return loanProductDao.findByProductId(Long.parseLong(productId));
    }

    @Override
    @Cacheable(value = "wln.loan.product.findAllProduct", unless = "#result == null")
    public Page<LoanProduct> findAllProduct() {
        // TODO Auto-generated method stub
        return loanProductDao.findAll(new PageRequest(0, countAllProduct()));
    }

    @Override
    public Page<LoanProduct> findAllProductPageable(PageRequest pageRequest) {
        // TODO Auto-generated method stub
        return loanProductDao.findAll(pageRequest);
    }

    @Override
    public List<ProductPlafond> findByProductId(LoanProduct productId) {
        // TODO Auto-generated method stub
        return productPlafondDao.findByProductIdOrderByPlafond(productId);
    }

    @Override
    public Page<BusinessType> findAllBusinessType() {
        // TODO Auto-generated method stub
        return businessTypeDao.findAll(new PageRequest(0, countAllBusinessType()));
    }

    @Override
    public Page<BusinessType> findAllBusinessTypePageable(PageRequest pageRequest) {
        // TODO Auto-generated method stub
        return businessTypeDao.findAll(pageRequest);
    }

    @Override
    public Page<BusinessTypeAP3R> findAllBusinessTypeAP3R() {
        return businessTypeAp3rDao.findAll(new PageRequest(0, countAllBusinessType()));
    }

    @Override
    public Page<BusinessTypeAP3R> findAllBusinessTypeAP3RPageable(PageRequest pageRequest) {
        return businessTypeAp3rDao.findAll(pageRequest);
    }

    @Override
    public Page<SurveyWawancara> findAllNonMs() {
        return swDao.findByStatusAndLocalIdIsNotNullAndIsDeleted(WebGuiConstant.STATUS_WAITING_APPROVAL, new PageRequest(0, countAllByStatus()), false);
    }

    @Override
    public Page<SurveyWawancara> findAllNonMsPageable(PageRequest pageRequest) {
        return swDao.findByStatusAndLocalIdIsNotNullAndIsDeleted(WebGuiConstant.STATUS_WAITING_APPROVAL, pageRequest, false);
    }

    @Override
    public Page<SurveyWawancara> findAllNonMsWithUserId(Long userId) {
        return swDao.findSWWaitingForApprovalNonMS(userId, WebGuiConstant.STATUS_WAITING_APPROVAL, new PageRequest(0, countAllByStatus()));
    }

    @Override
    public Page<SurveyWawancara> findAllNonMsWithUserIdPageable(Long userId, PageRequest pageRequest) {
        return swDao.findSWWaitingForApprovalNonMS(userId, WebGuiConstant.STATUS_WAITING_APPROVAL, pageRequest);
    }

    @Override
    public Page<SurveyWawancara> findByCreatedDateNonMs(Date startDate, Date endDate) {
        Calendar cal = Calendar.getInstance();
        Calendar calStart = Calendar.getInstance();
        cal.setTime(endDate);
        calStart.setTime(startDate);
        cal.add(Calendar.DATE, 1);
        calStart.add(Calendar.DATE, -7);
        return swDao.findByCreatedDateAndStatus(WebGuiConstant.STATUS_WAITING_APPROVAL, calStart.getTime(), cal.getTime(), new PageRequest(0, Integer.MAX_VALUE));
    }

    @Override
    public Page<SurveyWawancara> findByCreatedDateNonMsPageable(Date startDate, Date endDate, PageRequest pageRequest) {
        Calendar cal = Calendar.getInstance();
        Calendar calStart = Calendar.getInstance();
        cal.setTime(endDate);
        calStart.setTime(startDate);
        cal.add(Calendar.DATE, 1);
        calStart.add(Calendar.DATE, -7);
        return swDao.findByCreatedDateAndStatus(WebGuiConstant.STATUS_WAITING_APPROVAL, calStart.getTime(), cal.getTime(), pageRequest);
    }

    @Override
    @Cacheable(value = "wln.sw.findByLocalId", unless = "#result == null")
    public SurveyWawancara findByLocalId(String localId) {
        return swDao.findByLocalId(localId);
    }

    @Override
    public SWProductMapping findProductMapByLocalId(String localId) {
        return swProductMapDao.findByLocalId(localId);
    }

    @Override
    public List<SurveyWawancara> findIsDeletedSwList() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.add(Calendar.DATE, -7);
        Date dateBefore7Days = cal.getTime();
        return swDao.findIsDeletedSwId(dateBefore7Days, new Date());
    }

	@Override
	public List<SurveyWawancara> findSwByCustomerId(Long customerId, String status) {
		return swDao.findSwByCustomerId(customerId, status);
	}

}