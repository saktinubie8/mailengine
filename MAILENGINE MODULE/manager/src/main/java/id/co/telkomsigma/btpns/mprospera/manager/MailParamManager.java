package id.co.telkomsigma.btpns.mprospera.manager;

import java.util.List;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;

import id.co.telkomsigma.btpns.mprospera.model.parameter.MailParameter;

public interface MailParamManager {
	
	@Cacheable(value="allParameter", unless="#result == null")
	List<MailParameter> getAll();
	
	@Cacheable(value="paramById", unless="#result == null")
	MailParameter getParamById(Long id);
	
	@Cacheable(value="paramByMessageParam", unless="#result == null")
	MailParameter getParamByName(String messageParam);
	
	@CacheEvict(value={"allParameter", "paramById", "paramByMessageParam"},allEntries=true,beforeInvocation = true)
	void clearCache();

}
