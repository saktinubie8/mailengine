package id.co.telkomsigma.btpns.mprospera.manager;

import java.util.List;

import id.co.telkomsigma.btpns.mprospera.model.parameter.MailToken;

public interface MailTokenManager {

	List<MailToken> findAll();
	
	MailToken findByToken(String Token);
	
	MailToken save(MailToken mailToken);

}
