package id.co.telkomsigma.btpns.mprospera.dao;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import id.co.telkomsigma.btpns.mprospera.model.sw.BusinessType;

public interface BusinessTypeDao extends JpaRepository<BusinessType, Long> {

    @Query("SELECT COUNT(b) FROM BusinessType b")
    int countAll();

    @Query("SELECT b FROM BusinessType b")
    Page<BusinessType> findAll(Pageable pageable);
    
    BusinessType findByBusinessId(Long id);
    
    @Query("SELECT b.businessId FROM BusinessType b")
    List<Long> findAllBusinessTypeId();
    

}