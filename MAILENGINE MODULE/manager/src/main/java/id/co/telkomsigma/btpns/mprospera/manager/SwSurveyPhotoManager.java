package id.co.telkomsigma.btpns.mprospera.manager;

import id.co.telkomsigma.btpns.mprospera.model.sw.SwSurveyPhoto;

public interface SwSurveyPhotoManager {

    SwSurveyPhoto getSwSurveyPhoto(Long swid);

}