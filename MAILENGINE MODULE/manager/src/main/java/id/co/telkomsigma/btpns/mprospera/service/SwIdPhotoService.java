package id.co.telkomsigma.btpns.mprospera.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import id.co.telkomsigma.btpns.mprospera.manager.SwIdPhotoManager;
import id.co.telkomsigma.btpns.mprospera.model.sw.SwIdPhoto;

@Service("swIdPhotoService")
public class SwIdPhotoService {

    @Autowired
    private SwIdPhotoManager swIdPhotoManager;

    public SwIdPhoto getSwIdPhoto(Long swid) {
        return swIdPhotoManager.getSwIdPhoto(swid);
    }

}