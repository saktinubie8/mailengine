package id.co.telkomsigma.btpns.mprospera.controller.webservice;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Pattern;
import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.controller.GenericController;
import id.co.telkomsigma.btpns.mprospera.dto.ApprovalVariable;
import id.co.telkomsigma.btpns.mprospera.dto.FrekwensiTransaksiDto;
import id.co.telkomsigma.btpns.mprospera.dto.MailDto;
import id.co.telkomsigma.btpns.mprospera.dto.RejectDto;
import id.co.telkomsigma.btpns.mprospera.dto.RiwayatPembiayaanDto;
import id.co.telkomsigma.btpns.mprospera.dto.SaldoTabunganDto;
import id.co.telkomsigma.btpns.mprospera.dto.ValidationDataDto;
import id.co.telkomsigma.btpns.mprospera.model.customer.Customer;
import id.co.telkomsigma.btpns.mprospera.model.location.Location;
import id.co.telkomsigma.btpns.mprospera.model.messageLogs.MessageLogs;
import id.co.telkomsigma.btpns.mprospera.model.parameter.MailParameter;
import id.co.telkomsigma.btpns.mprospera.model.parameter.MailToken;
import id.co.telkomsigma.btpns.mprospera.model.sw.BusinessType;
import id.co.telkomsigma.btpns.mprospera.model.sw.LoanProduct;
import id.co.telkomsigma.btpns.mprospera.model.sw.SWProductMapping;
import id.co.telkomsigma.btpns.mprospera.model.sw.SWUserBWMPMapping;
import id.co.telkomsigma.btpns.mprospera.model.sw.SurveyWawancara;
import id.co.telkomsigma.btpns.mprospera.model.sw.SwIdPhoto;
import id.co.telkomsigma.btpns.mprospera.model.sw.SwSurveyPhoto;
import id.co.telkomsigma.btpns.mprospera.model.terminal.Terminal;
import id.co.telkomsigma.btpns.mprospera.model.terminal.TerminalActivity;
import id.co.telkomsigma.btpns.mprospera.model.user.User;
import id.co.telkomsigma.btpns.mprospera.pojo.FinancingHistory;
import id.co.telkomsigma.btpns.mprospera.pojo.LimitTerpakai;
import id.co.telkomsigma.btpns.mprospera.pojo.LoanPrs;
import id.co.telkomsigma.btpns.mprospera.pojo.MappingScoring;
import id.co.telkomsigma.btpns.mprospera.pojo.PlafonHistory;
import id.co.telkomsigma.btpns.mprospera.pojo.PlafonMapping;
import id.co.telkomsigma.btpns.mprospera.pojo.SwPreviousIncome;
import id.co.telkomsigma.btpns.mprospera.pojo.TotalAngsuran;
import id.co.telkomsigma.btpns.mprospera.pojo.UserEmail;
import id.co.telkomsigma.btpns.mprospera.request.MailRequest;
import id.co.telkomsigma.btpns.mprospera.request.NotifRequest;
import id.co.telkomsigma.btpns.mprospera.request.RejectRequest;
import id.co.telkomsigma.btpns.mprospera.request.SWApprovalHistoryReq;
import id.co.telkomsigma.btpns.mprospera.request.SWApprovalMailRequest;
import id.co.telkomsigma.btpns.mprospera.response.MailResponse;
import id.co.telkomsigma.btpns.mprospera.response.SWResponse;
import id.co.telkomsigma.btpns.mprospera.service.BusinessTypeService;
import id.co.telkomsigma.btpns.mprospera.service.CustomerService;
import id.co.telkomsigma.btpns.mprospera.service.FinancingHistoryService;
import id.co.telkomsigma.btpns.mprospera.service.LoanProductService;
import id.co.telkomsigma.btpns.mprospera.service.LocationService;
import id.co.telkomsigma.btpns.mprospera.service.MailParameterService;
import id.co.telkomsigma.btpns.mprospera.service.MailSenderService;
import id.co.telkomsigma.btpns.mprospera.service.MailTokenService;
import id.co.telkomsigma.btpns.mprospera.service.PlafonMappingService;
import id.co.telkomsigma.btpns.mprospera.service.SWService;
import id.co.telkomsigma.btpns.mprospera.service.SwIdPhotoService;
import id.co.telkomsigma.btpns.mprospera.service.SwProductMapService;
import id.co.telkomsigma.btpns.mprospera.service.SwSurveyPhotoService;
import id.co.telkomsigma.btpns.mprospera.service.TerminalActivityService;
import id.co.telkomsigma.btpns.mprospera.service.TerminalService;
import id.co.telkomsigma.btpns.mprospera.service.UserService;
import id.co.telkomsigma.btpns.mprospera.util.JsonUtils;
import id.co.telkomsigma.btpns.mprospera.util.TransactionIdGenerator;

@Controller("webserviceSendMailController")
public class SendMailController extends GenericController {

    @Autowired
    private MailParameterService mailParamSvc;

    @Autowired
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;

    @Autowired
    private TerminalActivityService terminalActivityService;

    @Autowired
    private TerminalService terminalService;

    @Autowired
    private MailSenderService mailSenderService;

    @Autowired
    private MailTokenService mailTokenService;

    @Autowired
    private SWService sWService;

    @Autowired
    private UserService userService;

    @Autowired
    private BusinessTypeService businessTypeService;

    @Autowired
    private LocationService locationService;

    @Autowired
    private SwProductMapService swProductMapService;

    @Autowired
    private LoanProductService loanProductService;

    @Autowired
    private CustomerService customerService;

    @Autowired
    private PlafonMappingService plafonMappingService;

    @Autowired
    private FinancingHistoryService financingHistoryService;
    
	@Autowired
	private SwSurveyPhotoService swSurveyPhotoSvc;
	
	@Autowired
	private SwIdPhotoService swPhotoSvc;

    final Pattern EMAIL_REGEX = Pattern.compile("(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])", Pattern.CASE_INSENSITIVE);

    JsonUtils jsonUtils = new JsonUtils();
    DecimalFormatSymbols symbolsRp = setSymbols("Rp. ");
    DecimalFormatSymbols symbolsPercent = setSymbols("");
    SimpleDateFormat sdfGlobal = new SimpleDateFormat("dd/MM/yyyy");

    @Value("${mail.server}")
    String mailServer;

    @RequestMapping(value = WebGuiConstant.SEND_EMAIL_REQUEST, method = {RequestMethod.POST})
    public @ResponseBody
    MailResponse sendMail(@RequestBody final MailRequest request) {
        MailResponse response = new MailResponse();
        response.setResponseCode(WebGuiConstant.RC_SUCCESS);
        try {
            log.info("REQUEST FOR SENDING MAIL : " + jsonUtils.toJson(request));
            if (!isValidEmail(request.getEmailAddress())) {
                log.error("email address for username " + request.getApprovalUsername() + " is invalid");
                return getGeneralErrorResponse(response, WebGuiConstant.RC_GENERAL_ERROR, WebGuiConstant.MAIL_INVALID_ADDRESS);
            }
            User user = userService.findUserByUsername(request.getApprovalUsername());
            if (user == null) {
                log.error("username not found");
                return getGeneralErrorResponse(response, WebGuiConstant.RC_USERNAME_NOT_EXISTS, WebGuiConstant.MAIL_USERNAME_NOT_FOUND);
            }
            HashMap<String, String> mailParamHash = getMailParam();
    		SwIdPhoto swIdPhoto = swPhotoSvc.getSwIdPhoto(Long.valueOf(request.getSwId()));
    		SwSurveyPhoto swSurveyPhoto = swSurveyPhotoSvc.getSwSurveyPhoto(Long.valueOf(request.getSwId()));
    		
    		if(swIdPhoto == null || swSurveyPhoto == null) {
    			log.error("Foto Ktp dan / atau Foto Survey tidak ditemukan");
    			return getGeneralErrorResponse(response, WebGuiConstant.RC_GENERAL_ERROR, "Foto Ktp dan / atau Foto Survey tidak ditemukan");
    		}
    		
    		if(!checkBmRole(user.getRoleUser())) {
    			log.error("User bukan BM");
    			return getGeneralErrorResponse(response, WebGuiConstant.RC_GENERAL_ERROR, "User bukan BM");
    		}
    		
            ValidationDataDto validationData = new ValidationDataDto();
            try {
                validationData = setValidationData(request.getSwId());
                validationData.setApprovalUsername(request.getApprovalUsername());
                validationData.setCatatan(request.getNote());
                validationData.setIsInReview(request.getIsInReview());
                validationData.setKodeDeviasi(request.getKodeDeviasi() == null ? "-" : request.getKodeDeviasi());
            } catch (NullPointerException ex) {
                log.error(ex.getMessage());
                ex.printStackTrace();
                String errorCode = "xx";
                if ("sw id not found".equals(ex.getMessage())) {
                    errorCode = WebGuiConstant.RC_UNKNOWN_SW_ID;
                } else if ("user id not found".equals(ex.getMessage())) {
                    errorCode = WebGuiConstant.RC_USERNAME_NOT_EXISTS;
                } else if ("sentra id not found".equals(ex.getMessage())) {
                    errorCode = WebGuiConstant.RC_UNKNOWN_SENTRA_ID;
                } else if ("loan id not found".equals(ex.getMessage())) {
                    errorCode = WebGuiConstant.RC_UNKNOWN_LOAN_ID;
                } else if ("loan history not found".equals(ex.getMessage())) {
                    errorCode = WebGuiConstant.RC_NO_LOAN_HIST;
                } else {
                    errorCode = WebGuiConstant.RC_GENERAL_ERROR;
                }
                return getGeneralErrorResponse(response, errorCode, ex.getMessage());
            }
            
            if(isDraf(validationData.getStatus())) {
    			log.error("Status sw DRAF");
    			return getGeneralErrorResponse(response, WebGuiConstant.RC_GENERAL_ERROR, "Status sw DRAFT");
            }
            
            Date expiredDate = setExpiredDate(mailParamHash.get(WebGuiConstant.MAIL_DATE_FORMAT), mailParamHash.get(WebGuiConstant.MAIL_DATE_EXPIRED));
            String subject;
            if (request.getIsForwarded()) {
                subject = mailParamHash.get(WebGuiConstant.MAIL_SUBJECT_FORWARDED);
            } else {
                subject = mailParamHash.get(WebGuiConstant.MAIL_SUBJECT);
            }
            MailDto mail = mailPack(request.getSwId(), subject, mailServer, request.getEmailAddress(), request.getUserName(), mailParamHash.get(WebGuiConstant.MAIL_DOMAIN), swIdPhoto.getIdPhoto(), swSurveyPhoto.getSurveyPhoto());
            mailParamHash.clear();
            try {
                mailSenderService.sendEMail(mail, request.getIsForwarded(), expiredDate, validationData);
                log.debug("mail send to " + request.getEmailAddress());
                response.setResponseMessage("send mail success");
            } catch (MessagingException | IOException ex) {
                log.error(ex.getMessage());
                ex.printStackTrace();
                setErrorResponse(response, WebGuiConstant.RC_GENERAL_ERROR, ex.getMessage());
            }
        } catch (Exception e) {
            log.error(e.getMessage());
            e.printStackTrace();
            setErrorResponse(response, WebGuiConstant.RC_GENERAL_ERROR, e.getMessage());
        } finally {
            try {
                // insert MessageLogs
                log.debug("Trying to CREATE Terminal Activity...");
                threadPoolTaskExecutor.execute(new Runnable() {
                    @Override
                    public void run() {
                        Terminal terminal = terminalService.loadTerminalByImei("358525072034683");
                        TerminalActivity terminalActivity = new TerminalActivity();
                        // Logging to terminal activity
                        terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
                        terminalActivity.setActivityType(TerminalActivity.ACTIVITY_SEND_MAIL);
                        terminalActivity.setCreatedDate(new Date());
                        terminalActivity.setReffNo(request.getRetrievalReferenceNumber().trim());
                        terminalActivity.setSessionKey("");
                        terminalActivity.setTerminal(terminal);
                        terminalActivity.setUsername("System");
                        List<MessageLogs> messageLogs = new ArrayList<>();
                        terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
                        log.debug("TERMINAL ACTIVITY LOGGING SUCCESS...");
                        log.debug("Updating Terminal Activity");
                    }
                });
                log.info("RESPONSE MESSAGE : " + jsonUtils.toJson(response));
            } catch (Exception e) {
                log.error("ERROR CREATE terminalActivity : " + e.getMessage());
            }
        }
        return response;
    }

    @RequestMapping(value = WebGuiConstant.SEND_NOTIF_REQUEST, method = {RequestMethod.POST})
    public @ResponseBody
    MailResponse sendNotif(@RequestBody final NotifRequest request) {
        MailResponse response = new MailResponse();
        response.setResponseCode(WebGuiConstant.RC_SUCCESS);
        try {
            log.info("INCOMING MESSAGE : " + jsonUtils.toJson(request));
            if (!isValidEmail(request.getEmailAddress())) {
                log.error("email address for username " + request.getUserName() + " is invalid");
                return getGeneralErrorResponse(response, WebGuiConstant.RC_GENERAL_ERROR, WebGuiConstant.MAIL_INVALID_ADDRESS);
            }
            HashMap<String, String> mailParamHash = getMailParam();
            MailDto mail = mailPack(mailParamHash.get(WebGuiConstant.MAIL_SUBJECT_NOTIF), request.getEmailAddress(), mailServer, request.getRecieverName());
            mailParamHash.clear();
            try {
                mailSenderService.sendNotificationMail(mail, request.getCustomerName());
                log.debug("Notification send to " + request.getEmailAddress());
                response.setResponseMessage("send notif mail success");
            } catch (MessagingException | IOException ex) {
                log.error(ex.getMessage());
                ex.printStackTrace();
                setErrorResponse(response, WebGuiConstant.RC_GENERAL_ERROR, ex.getMessage());
            }
        } catch (Exception e) {
            log.error(e.getMessage());
            e.printStackTrace();
            setErrorResponse(response, WebGuiConstant.RC_GENERAL_ERROR, e.getMessage());
        } finally {
            try {
                // insert MessageLogs
                log.debug("Trying to CREATE Terminal Activity...");
                threadPoolTaskExecutor.execute(new Runnable() {
                    @Override
                    public void run() {
                        Terminal terminal = terminalService.loadTerminalByImei("358525072034683");
                        TerminalActivity terminalActivity = new TerminalActivity();
                        // Logging to terminal activity
                        terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
                        terminalActivity.setActivityType(TerminalActivity.ACTIVITY_SEND_MAIL);
                        terminalActivity.setCreatedDate(new Date());
                        terminalActivity.setReffNo(request.getRetrievalReferenceNumber().trim());
                        terminalActivity.setSessionKey("");
                        terminalActivity.setTerminal(terminal);
                        terminalActivity.setUsername("System");
                        List<MessageLogs> messageLogs = new ArrayList<>();
                        terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
                        log.debug("TERMINAL ACTIVITY LOGGING SUCCESS...");
                        log.debug("Updating Terminal Activity");
                    }
                });
                log.info("RESPONSE MESSAGE : " + jsonUtils.toJson(response));
            } catch (Exception e) {
                log.error("ERROR CREATE terminalActivity : " + e.getMessage());
            }
        }
        return response;
    }

    @RequestMapping(value = WebGuiConstant.SEND_NOTIF_REJECT_SCORING_REQUEST, method = {RequestMethod.POST})
    public @ResponseBody
    MailResponse sendNotifReject(@RequestBody final RejectRequest request) {
        MailResponse response = new MailResponse();
        response.setResponseCode(WebGuiConstant.RC_SUCCESS);
        try {
            User user = userService.findUserByUsername(request.getUserName());
            if (user == null) {
                log.error("username not found");
                return getGeneralErrorResponse(response, WebGuiConstant.RC_USERNAME_NOT_EXISTS, WebGuiConstant.MAIL_USERNAME_NOT_FOUND);
            }
            boolean isBm = checkBmRole(user.getRoleUser());
            log.info("REQUEST FOR SW REJECTED NOTIF : " + jsonUtils.toJson(request));
            if (!isValidEmail(user.getEmail())) {
                log.error("email address for username " + request.getUserName() + " is invalid");
                return getGeneralErrorResponse(response, WebGuiConstant.RC_GENERAL_ERROR, WebGuiConstant.MAIL_INVALID_ADDRESS);
            }
            RejectDto mail = new RejectDto();
            HashMap<String, String> mailParamHash = getMailParam();
            if (isBm) {
                mail = mailRejectPack(mailParamHash.get(WebGuiConstant.MAIL_SUBJECT_NOTIF), user.getEmail(), mailServer, request.getCustomerName(), request.getAppidNo(), request.getSentra(), request.getAlasan());
                mail.setMms(request.getMms());
            } else {
                mail = mailRejectPack(mailParamHash.get(WebGuiConstant.MAIL_SUBJECT_NOTIF), user.getEmail(), mailServer, request.getCustomerName(), request.getAppidNo(), request.getSentra(), request.getAlasan());
            }
            mailParamHash.clear();
            try {
                mailSenderService.sendNotificationRejectMail(mail, isBm, true);
                log.debug("Reject Notification send to " + user.getEmail());
                response.setResponseMessage("send notif mail success");
            } catch (MessagingException | IOException ex) {
                log.error(ex.getMessage());
                ex.printStackTrace();
                setErrorResponse(response, WebGuiConstant.RC_GENERAL_ERROR, ex.getMessage());
            }
        } catch (Exception e) {
            log.error(e.getMessage());
            e.printStackTrace();
            setErrorResponse(response, WebGuiConstant.RC_GENERAL_ERROR, e.getMessage());
        } finally {
            try {
                // insert MessageLogs
                log.debug("Trying to CREATE Terminal Activity...");
                threadPoolTaskExecutor.execute(new Runnable() {
                    @Override
                    public void run() {
                        Terminal terminal = terminalService.loadTerminalByImei("358525072034683");
                        TerminalActivity terminalActivity = new TerminalActivity();
                        // Logging to terminal activity
                        terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
                        terminalActivity.setActivityType(TerminalActivity.ACTIVITY_SEND_MAIL);
                        terminalActivity.setCreatedDate(new Date());
                        terminalActivity.setReffNo(request.getRetrievalReferenceNumber().trim());
                        terminalActivity.setSessionKey("");
                        terminalActivity.setTerminal(terminal);
                        terminalActivity.setUsername("System");
                        List<MessageLogs> messageLogs = new ArrayList<>();
                        terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
                        log.info("TERMINAL ACTIVITY LOGGING SUCCESS...");
                        log.info("Updating Terminal Activity");
                    }
                });
                log.info("RESPONSE MESSAGE : " + jsonUtils.toJson(response));
            } catch (Exception e) {
                log.error("ERROR CREATE terminalActivity : " + e.getMessage());
            }
        }
        return response;
    }
    
    @RequestMapping(value = WebGuiConstant.SEND_NOTIF_DATA_NOT_COMPLETE, method= { RequestMethod.POST})
	public @ResponseBody MailResponse sendNotifDataNotComplete(@RequestBody final RejectRequest request) {
		MailResponse response = new MailResponse();
		response.setResponseCode(WebGuiConstant.RC_SUCCESS);
		try {
				SurveyWawancara sw = sWService.findByRrn(request.getRetrievalReferenceNumber());
				if(isDraf(sw.getStatus())) {
					log.error("Status sw DRAF");
	    			return getGeneralErrorResponse(response, WebGuiConstant.RC_GENERAL_ERROR, "Status sw DRAFT");
				}
				
				User user =  userService.findUserByUsername(request.getUserName());
				if(user == null) {
					log.error("username not found");
					return getGeneralErrorResponse(response,WebGuiConstant.RC_USERNAME_NOT_EXISTS,WebGuiConstant.MAIL_USERNAME_NOT_FOUND);
				}
				boolean isBm = checkBmRole(user.getRoleUser());
				log.info("REQUEST FOR MISSING IMAGE NOTIF : " + jsonUtils.toJson(request));
				if(!isValidEmail(user.getEmail())) {
					log.error("email address for username "+request.getUserName()+" is invalid");
					return getGeneralErrorResponse(response,WebGuiConstant.RC_GENERAL_ERROR,WebGuiConstant.MAIL_INVALID_ADDRESS);
				}

				RejectDto mail = new RejectDto();
				HashMap<String,String> mailParamHash = getMailParam();
				if(isBm) {
					mail = mailRejectPack(mailParamHash.get(WebGuiConstant.MAIL_SUBJECT_NOTIF), user.getEmail(), mailServer,  request.getCustomerName(), request.getAppidNo(), request.getSentra(), "-");
					mail.setMms(request.getMms());
				}else {
					mail = mailRejectPack(mailParamHash.get(WebGuiConstant.MAIL_SUBJECT_NOTIF), user.getEmail(), mailServer,  request.getCustomerName(), request.getAppidNo(), request.getSentra(), "-");
				}
				mailParamHash.clear();
				try {
					mailSenderService.sendNotificationRejectMail(mail, isBm,false);
					log.info("Reject Notification send to "+user.getEmail());
					response.setResponseMessage("send notif mail success");
				}catch (MessagingException | IOException ex) {
					log.error(ex.getMessage());
                    ex.printStackTrace();
                    setErrorResponse(response,WebGuiConstant.RC_GENERAL_ERROR,ex.getMessage());
				}
			
		} catch (Exception e) {
			log.error(e.getMessage());
            e.printStackTrace();
            setErrorResponse(response, WebGuiConstant.RC_GENERAL_ERROR, e.getMessage());
        } finally {
            try {
                // insert MessageLogs
                log.info("Trying to CREATE Terminal Activity...");
                threadPoolTaskExecutor.execute(new Runnable() {
                    @Override
                    public void run() {
                        Terminal terminal = terminalService.loadTerminalByImei("358525072034683");
                        TerminalActivity terminalActivity = new TerminalActivity();
                        // Logging to terminal activity
                        terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
                        terminalActivity.setActivityType(TerminalActivity.ACTIVITY_SEND_MAIL);
                        terminalActivity.setCreatedDate(new Date());
                        terminalActivity.setReffNo(request.getRetrievalReferenceNumber().trim());
                        terminalActivity.setSessionKey("");
                        terminalActivity.setTerminal(terminal);
                        terminalActivity.setUsername("System");
                        List<MessageLogs> messageLogs = new ArrayList<>();
                        terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
                        log.info("TERMINAL ACTIVITY LOGGING SUCCESS...");
                        log.info("Updating Terminal Activity");
                    }
                });
                log.info("RESPONSE MESSAGE : " + jsonUtils.toJson(response));
            } catch (Exception e) {
                log.error("ERROR CREATE terminalActivity : " + e.getMessage());
            }
        }
        return response;
    }

    @RequestMapping(value = "/webservice/landingPage", method = RequestMethod.GET)
    public String landingPage(@RequestParam(name = "token") String token,
                              @RequestParam(name = "status") String status,
                              @RequestParam(name = "username") String username,
                              final Model model, final HttpServletRequest request,
                              final HttpServletResponse response) {
        try {
            MailToken mailToken = mailTokenService.findByToken(token);
            log.debug("token :" + token + " status: " + status + " username: " + username);
            //check if token is not found
            if (mailToken == null) {
                log.debug("Mailtoken null");
                return getGeneralErrorPage(WebGuiConstant.MAIL_INVALID_TOKEN, model);
            }
            //check if request is already approved / rejected
            SurveyWawancara sw = sWService.getSWById(mailToken.getSwId());
            if (isAlreadyDecided(sw.getStatus())) {
                if (tokenNotExpired(mailToken.getIsExpired())) {
                    setUsedToken(mailToken, username, WebGuiConstant.STATUS_EXPIRED);
                }
                return getAlreadyDecidedErrorPage(sw.getStatus(), sw.getUpdatedBy(), model);
            }
            //check if token is expired
            if (mailToken.getIsExpired().equals("0") && !isDateExpired(mailToken.getExpiredDate())) {
                //begin action
                if (status.equals("1")) {
                    model.addAttribute("custName", sw.getCustomerName());
                    model.addAttribute("token", token);
                    model.addAttribute("approvalUsername", username);
                    return WebGuiConstant.MAIL_APPROVE_CONFIRM_PATH;
                } else if (status.equals("2")) {
                    model.addAttribute("token", token);
                    model.addAttribute("username", username);
                    return WebGuiConstant.MAIL_REJECT_REASON_PATH; //rejection
                } else if (status.equals("3")) {
                    long swId = Long.parseLong(mailToken.getSwId());
                    List<UserEmail> userList = userService.findUserByLimit(swId);
                    HashMap<String, String> emails = new HashMap<String, String>();
                    for (UserEmail user : userList) {
                        emails.put(user.getFullName(), user.getUserLogin()+","+user.getEmail());
                    }
                    model.addAttribute("emails", emails);
                    model.addAttribute("token", token);
                    model.addAttribute("senderUserName", username);
                    return WebGuiConstant.MAIL_FORWARD_PATH; //forward
                } else {
                    return getGeneralErrorPage(WebGuiConstant.MAIL_UNKNOWN_STATUS, model);
                }
            } else if (mailToken.getIsExpired().equals("0") && isDateExpired(mailToken.getExpiredDate())) {
                setUsedToken(mailToken, WebGuiConstant.MAIL_SYSTEM, WebGuiConstant.STATUS_EXPIRED);
              	log.debug("token "+mailToken.getToken()+" expired");
                return getGeneralErrorPage(WebGuiConstant.MAIL_TOKEN_EXPIRED, model);
            } else {
              	log.debug("token "+mailToken.getToken()+" expired");
                return getGeneralErrorPage(WebGuiConstant.MAIL_TOKEN_EXPIRED, model);
            }
        } catch (Exception e) {
            log.error(e.getMessage());
            e.printStackTrace();
            return getGeneralErrorPage(e.getMessage(), model);
        }
    }

    @RequestMapping(value = "/webservice/approveLandingPage", method = RequestMethod.GET)
    public String approvalLandingPage(@RequestParam(name = "username") String username, @RequestParam(name = "token") String token,
                                      final Model model, final HttpServletRequest request,
                                      final HttpServletResponse response) {
        try {
            MailToken mailToken = mailTokenService.findByToken(token);
            if (mailToken.getIsExpired().equals("0") && !isDateExpired(mailToken.getExpiredDate())) {
                List<SWApprovalHistoryReq> listHistory = getUserBwmpMap(Long.parseLong(mailToken.getSwId()),username,false);
                String approveReq = approvalRequestPack(WebGuiConstant.STATUS_APPROVED, mailToken.getSwId(), username, listHistory);
                log.info("approval request : "+approveReq);
                SWResponse sWResponse = sWService.approveSW(approveReq);
                if (sWResponse.getResponseCode().equals(WebGuiConstant.RC_SUCCESS)) {
                    setUsedToken(mailToken, username, WebGuiConstant.STATUS_APPROVED);
                    log.debug("Approval for sw "+mailToken.getSwId()+" success");
                    return WebGuiConstant.MAIL_SUCCESS_PAGE_PATH; //approved
                } else if (sWResponse.getResponseCode().equals(WebGuiConstant.RC_UNKNOWN_PHOTO_SW_ID)) {
                	log.debug("Image id / survey not found for sw "+mailToken.getSwId());
                    return WebGuiConstant.MAIL_ERROR_IMG_NOT_PROCESSED_PATH;
                } else {
                	log.debug("Approval for sw "+mailToken.getSwId()+" failed, because "+sWResponse.getResponseMessage());
                    return getGeneralErrorPage(sWResponse.getResponseMessage(), model);
                }
            } else if (mailToken.getIsExpired().equals("0") && isDateExpired(mailToken.getExpiredDate())) {
                setUsedToken(mailToken, WebGuiConstant.MAIL_SYSTEM, WebGuiConstant.STATUS_EXPIRED);
              	log.debug("token "+mailToken.getToken()+" expired");
                return getGeneralErrorPage(WebGuiConstant.MAIL_TOKEN_EXPIRED, model);
            } else {
            	log.debug("token "+mailToken.getToken()+" expired");
                return getGeneralErrorPage(WebGuiConstant.MAIL_TOKEN_EXPIRED, model);
            }
        } catch (Exception e) {
            log.error(e.getMessage());
            e.printStackTrace();
            return getGeneralErrorPage(e.getMessage(), model);
        }
    }

    @RequestMapping(value = "/webservice/forwardLandingPage", method = {RequestMethod.POST})
    public String forwardLandingPage(@RequestParam(value = "email") String email, @RequestParam(name = "token") String token,
                                     @RequestParam(name = "senderUserName") String senderUserName, final Model model, final HttpServletRequest request, final HttpServletResponse response) {
        try {
            MailToken mailToken = mailTokenService.findByToken(token);
            if (mailToken.getIsExpired().equals("0") && !isDateExpired(mailToken.getExpiredDate())) {
                boolean isForwarded = false;
                String[] mailArray = email.split(",");
                String username = mailArray[0];
                String mailAddress = mailArray[1];
                if (!isValidEmail(mailAddress)) {
                    log.error("email address for username " + username + " is invalid");
                    return getGeneralErrorPage(WebGuiConstant.MAIL_INVALID_ADDRESS, model);
                }
                List<SWApprovalHistoryReq> listHistory = getUserBwmpMap(Long.parseLong(mailToken.getSwId()),username,true);
                String forwardReq = approvalRequestPack(WebGuiConstant.STATUS_WAITING_APPROVAL, mailToken.getSwId(), username, listHistory);
                log.info("forward request : "+forwardReq);
                SWResponse sWResponse = sWService.approveSW(forwardReq);
                if (sWResponse.getResponseCode().equals(WebGuiConstant.RC_SUCCESS)) {
                	log.debug("Approval BM for sw "+mailToken.getSwId()+" success, Forwarding message to "+username);
                    HashMap<String, String> mailParamHash = getMailParam();
                    SwIdPhoto swIdPhoto = swPhotoSvc.getSwIdPhoto(Long.valueOf(mailToken.getSwId()));
                    SwSurveyPhoto swSurveyPhoto = swSurveyPhotoSvc.getSwSurveyPhoto(Long.valueOf(mailToken.getSwId()));
                    if (swIdPhoto == null || swSurveyPhoto == null) {
                        log.error("Foto Ktp dan / atau Foto Survey tidak ditemukan");
                        return getGeneralErrorPage("Foto Ktp dan / atau Foto Survey tidak ditemukan", model);
                    }
                    ValidationDataDto validationData = new ValidationDataDto();
                    try {
                        validationData = setValidationData(mailToken.getSwId());
                        validationData.setApprovalUsername(username);
                        validationData.setCatatan(mailToken.getNote());
                        validationData.setIsInReview(mailToken.getInReview());
                        validationData.setKodeDeviasi(mailToken.getDeviationCode());

                    } catch (NullPointerException ex) {
                        log.error(ex.getMessage());
                        ex.printStackTrace();
                        return getGeneralErrorPage(ex.getMessage(), model);
                    }
                    Date expiredDate = setExpiredDate(mailParamHash.get(WebGuiConstant.MAIL_DATE_FORMAT), mailParamHash.get(WebGuiConstant.MAIL_DATE_EXPIRED));
                    MailDto mail = mailPack(mailToken.getSwId(), mailParamHash.get(WebGuiConstant.MAIL_SUBJECT), mailServer, mailAddress, senderUserName, mailParamHash.get(WebGuiConstant.MAIL_DOMAIN), swIdPhoto.getIdPhoto(), swSurveyPhoto.getSurveyPhoto());
                    mailParamHash.clear();
                    try {
                        mailSenderService.sendEMail(mail, isForwarded, expiredDate, validationData);
                        log.debug("Mail forwarded to " + mailAddress);
                    } catch (MessagingException | IOException ex) {
                        log.error(ex.getMessage());
                        ex.printStackTrace();
                        return getGeneralErrorPage(ex.getMessage(), model);
                    }
                    setUsedToken(mailToken, senderUserName, WebGuiConstant.STATUS_FORWARDED);
                    model.addAttribute("email", mailAddress);
                    return WebGuiConstant.MAIL_FWD_SUCCESS_PAGE_PATH;
                } else if (sWResponse.getResponseCode().equals(WebGuiConstant.RC_UNKNOWN_PHOTO_SW_ID)) {
                	log.debug("Image id / survey not found for sw "+mailToken.getSwId());
                    return WebGuiConstant.MAIL_ERROR_IMG_NOT_PROCESSED_PATH;
                } else {
                	log.debug("Approval BM for sw "+mailToken.getSwId()+" failed, because "+sWResponse.getResponseMessage());
                    return getGeneralErrorPage(sWResponse.getResponseMessage(), model);
                }
            } else if (mailToken.getIsExpired().equals("0") && isDateExpired(mailToken.getExpiredDate())) {
                setUsedToken(mailToken, WebGuiConstant.MAIL_SYSTEM, WebGuiConstant.STATUS_EXPIRED);
              	log.debug("token "+mailToken.getToken()+" expired");
                return getGeneralErrorPage(WebGuiConstant.MAIL_TOKEN_EXPIRED, model);
            } else {
              	log.debug("token "+mailToken.getToken()+" expired");
                return getGeneralErrorPage(WebGuiConstant.MAIL_TOKEN_EXPIRED, model);
            }
        } catch (Exception e) {
            log.error(e.getMessage());
            e.printStackTrace();
            return getGeneralErrorPage(e.getMessage(), model);
        }
    }

    @RequestMapping(value = "/webservice/returnReasonPage", method = RequestMethod.GET)
    public String returnReasonPage(@RequestParam(name = "namaNasabah") String namaNasabah,
                                   @RequestParam(name = "token") String token, @RequestParam(name = "username") String username,
                                   final Model model, final HttpServletRequest request, final HttpServletResponse response) {
        try {
            MailToken mailToken = mailTokenService.findByToken(token);
            if (mailToken == null) {
                return getGeneralErrorPage(WebGuiConstant.MAIL_INVALID_TOKEN, model);
            }
            //check if request is already approved / rejected
            SurveyWawancara sw = sWService.getSWById(mailToken.getSwId());
            if (isAlreadyDecided(sw.getStatus())) {
                if (tokenNotExpired(mailToken.getIsExpired())) {
                    setUsedToken(mailToken, username,WebGuiConstant.STATUS_EXPIRED);
                }
                return getAlreadyDecidedErrorPage(sw.getStatus(), sw.getUpdatedBy(), model);
            }
            if (mailToken.getIsExpired().equals("0") && !isDateExpired(mailToken.getExpiredDate())) {
                User user = userService.findUserByUsername(mailToken.getCreatedBy());
                model.addAttribute("returnAddress", user.getEmail());
                model.addAttribute("token", token);
                model.addAttribute("fromAddress", mailToken.getSendTo());
                model.addAttribute("username", username);
                model.addAttribute("namaNasabah", namaNasabah);
                return WebGuiConstant.MAIL_RETURN_REASON_PATH;
            } else if (mailToken.getIsExpired().equals("0") && isDateExpired(mailToken.getExpiredDate())) {
                setUsedToken(mailToken, WebGuiConstant.MAIL_SYSTEM, WebGuiConstant.STATUS_EXPIRED);
              	log.debug("token "+mailToken.getToken()+" expired");
                return getGeneralErrorPage(WebGuiConstant.MAIL_TOKEN_EXPIRED, model);
            } else {
              	log.debug("token "+mailToken.getToken()+" expired");
                return getGeneralErrorPage(WebGuiConstant.MAIL_TOKEN_EXPIRED, model);
            }
        } catch (Exception e) {
            log.error(e.getMessage());
            e.printStackTrace();
            return getGeneralErrorPage(e.getMessage(), model);
        }
    }

    @RequestMapping(value = "/webservice/returnLandingPage", method = {RequestMethod.POST})
    public String returnLandingPage(@RequestParam(name = "fromAddress") String fromAddress, @RequestParam(name = "alasan") String alasan,
                                    @RequestParam(name = "returnAddress") String returnAddress, @RequestParam(name = "namaNasabah") String namaNasabah,
                                    @RequestParam(name = "token") String token, @RequestParam(name = "username") String username,
                                    final Model model, final HttpServletRequest request, final HttpServletResponse response) {
        try {
            MailToken mailToken = mailTokenService.findByToken(token);
            if (mailToken.getIsExpired().equals("0") && !isDateExpired(mailToken.getExpiredDate())) {
                if (!isValidEmail(returnAddress)) {
                    log.error("email address for username " + username + " is invalid");
                    return getGeneralErrorPage(WebGuiConstant.MAIL_INVALID_ADDRESS, model);
                }
                List<SWApprovalHistoryReq> listHistory = getUserBwmpMap(Long.parseLong(mailToken.getSwId()),username,false);
                String returnReq = approvalRequestPack(WebGuiConstant.STATUS_WAITING_APPROVAL, mailToken.getSwId(), username, listHistory);
                log.info("return request : "+returnReq);
                SWResponse sWResponse = sWService.approveSW(returnReq);
                if (sWResponse.getResponseCode().equals(WebGuiConstant.RC_SUCCESS)) {
                	log.debug("Sending return mail for sw "+mailToken.getSwId()+" to "+returnAddress);
                    HashMap<String, String> param = getMailParam();
                    User user = userService.findUserByUsername(mailToken.getCreatedBy());
                    MailDto mailDto = mailPack(param.get(WebGuiConstant.MAIL_SUBJECT_RETURN), returnAddress, mailServer, user.getName());
                    param.clear();
                    try {
                        mailSenderService.sendNotificationReturnMail(mailDto, fromAddress, namaNasabah, alasan);
                        log.debug("Mail Returned to " + returnAddress);
                    } catch (MessagingException | IOException ex) {
                        log.error(ex.getMessage());
                        ex.printStackTrace();
                        return getGeneralErrorPage(ex.getMessage(), model);
                    }
                    setUsedToken(mailToken, username, WebGuiConstant.STATUS_RETURNED);
                    model.addAttribute("namaNasabah", namaNasabah);
                    model.addAttribute("returnAddress", returnAddress);
                    return WebGuiConstant.MAIL_RETURN_PATH;
                } else if (sWResponse.getResponseCode().equals(WebGuiConstant.RC_UNKNOWN_PHOTO_SW_ID)) {
                	log.debug("Image id / survey not found for sw "+mailToken.getSwId());
                    return WebGuiConstant.MAIL_ERROR_IMG_NOT_PROCESSED_PATH;
                } else {
                	log.debug("Returning mail for sw "+mailToken.getSwId()+" failed, because "+sWResponse.getResponseMessage());
                    return getGeneralErrorPage(sWResponse.getResponseMessage(), model);
                }
            } else if (mailToken.getIsExpired().equals("0") && isDateExpired(mailToken.getExpiredDate())) {
                setUsedToken(mailToken, WebGuiConstant.MAIL_SYSTEM, WebGuiConstant.STATUS_EXPIRED);
              	log.debug("token "+mailToken.getToken()+" expired");
                return getGeneralErrorPage(WebGuiConstant.MAIL_TOKEN_EXPIRED, model);
            } else {
              	log.debug("token "+mailToken.getToken()+" expired");
                return getGeneralErrorPage(WebGuiConstant.MAIL_TOKEN_EXPIRED, model);
            }
        } catch (Exception e) {
            log.error(e.getMessage());
            e.printStackTrace();
            return getGeneralErrorPage(e.getMessage(), model);
        }
    }

    @RequestMapping(value = "/webservice/rejectLandingPage", method = {RequestMethod.POST})
    public String rejectLandingPage(@RequestParam(name = "token") String token,
                                    @RequestParam(name = "alasanpenolakan") String alasanPenolakan,
                                    @RequestParam(name = "username") String username,
                                    final Model model, final HttpServletRequest request, final HttpServletResponse response) {
        try {
            MailToken mailToken = mailTokenService.findByToken(token);
            if (mailToken.getIsExpired().equals("0") && !isDateExpired(mailToken.getExpiredDate())) {
            	String rejectReq = rejectRequestPack(mailToken.getSwId(), username, alasanPenolakan);
            	log.info("reject request : "+rejectReq);
                SWResponse sWResponse = sWService.approveSW(rejectReq);
                if (sWResponse.getResponseCode().equals(WebGuiConstant.RC_SUCCESS)) {
                	log.debug("Reject request for sw "+mailToken.getSwId()+" success");
                    setUsedToken(mailToken, username, WebGuiConstant.STATUS_REJECTED);
                    model.addAttribute("alasanpenolakan", alasanPenolakan);
                    return WebGuiConstant.MAIL_REJECT_SUCCESS_PATH;
                } else if (sWResponse.getResponseCode().equals(WebGuiConstant.RC_UNKNOWN_PHOTO_SW_ID)) {
                	log.debug("Image id / survey not found for sw "+mailToken.getSwId());
                    return WebGuiConstant.MAIL_ERROR_IMG_NOT_PROCESSED_PATH;
                } else {
                	log.debug("Reject request for sw "+mailToken.getSwId()+" failed, because "+sWResponse.getResponseMessage());
                    return getGeneralErrorPage(sWResponse.getResponseMessage(), model);
                }
            } else if (mailToken.getIsExpired().equals("0") && isDateExpired(mailToken.getExpiredDate())) {
                setUsedToken(mailToken, WebGuiConstant.MAIL_SYSTEM, WebGuiConstant.STATUS_EXPIRED);
              	log.debug("token "+mailToken.getToken()+" expired");
                return getGeneralErrorPage(WebGuiConstant.MAIL_TOKEN_EXPIRED, model);
            } else {
              	log.debug("token "+mailToken.getToken()+" expired");
                return getGeneralErrorPage(WebGuiConstant.MAIL_TOKEN_EXPIRED, model);
            }
        } catch (Exception e) {
            log.error(e.getMessage());
            e.printStackTrace();
            return getGeneralErrorPage(e.getMessage(), model);
        }
    }

    /**
     * Set all required variable for email body
     *
     * @param swId swId
     * @return all required variable for email body
     * @author kusnendi
     */
    private ValidationDataDto setValidationData(String swId) throws NullPointerException {
        ValidationDataDto val = new ValidationDataDto();
        DecimalFormat df = (DecimalFormat) DecimalFormat.getCurrencyInstance();
        df.setDecimalSeparatorAlwaysShown(false);
        // Chained query
        SurveyWawancara sw = sWService.getSWById(swId);
        if (sw == null) {
            throw new NullPointerException("sw id not found");
        }
        // flag untuk mengecek apakah totalIncome nya 0
        boolean isZeroTotalIncome = checkZeroTotalIncome(sw.getTotalIncome());
        boolean isZeroPbu = false;
        boolean isNewCustomer = checkNewCustomer(sw.getCustomerRegistrationType().toString());
        User user = userService.findUserByUsername(sw.getCreatedBy());
        if (user == null) {
            throw new NullPointerException("user id not found");
        }
        Location location = locationService.findByLocationId(user.getOfficeCode());
        if (location == null) {
            throw new NullPointerException("sentra id not found");
        }
        SWProductMapping sWProductMapping = swProductMapService.findOneSwProductMapBySwId(Long.valueOf(swId));
        if (sWProductMapping == null) {
            throw new NullPointerException("product mapping not found");
        }
        LoanProduct loanProduct = loanProductService.findByProductId(sWProductMapping.getRecommendedProductId());
        if (loanProduct == null) {
            throw new NullPointerException("loan id not found");
        }
        /*
         * Jika pbu di bawah 2 jt (total income nya 0) maka cari pbu sebelumnya yang
         * total income nya tidak 0 dan telah di approve, Jika tidak ada maka set semua
         * field yang membutuhkan perhitungan pembagian menjadi 0 Agar terhindar dari
         * pembagian dengan nol
         */
        if (isZeroTotalIncome) {
            if (!isNewCustomer) {
                List<SurveyWawancara> swList = sWService.findSwByCustomer(sw.getCustomerId(),
                        WebGuiConstant.STATUS_APPROVED);
                if (!swList.isEmpty()) {
                    boolean isNotZero = false;
                    for (SurveyWawancara survey : swList) {
                        if (survey.getTotalIncome().compareTo(BigDecimal.ZERO) == 0) {
                            continue;
                        } else {
                            isNotZero = true;
                            sw = survey;
                            sWProductMapping = swProductMapService.findOneSwProductMapBySwId(sw.getSwId());
                            loanProduct = loanProductService
                                    .findByProductId(sWProductMapping.getRecommendedProductId());
                            break;
                        }
                    }
                    if (!isNotZero) {
                        isZeroPbu = true;
                    }
                } else {
                    isZeroPbu = true;
                }
            } else {
                isZeroPbu = true;
            }
        }
        // query parameter
        List<Long> businessTypeIds = businessTypeService.findAllBusinessTypeId();
        // custom query
        List<TotalAngsuran> totalAngsuranList = new ArrayList<TotalAngsuran>();
        List<SwPreviousIncome> previousIncomeList = new ArrayList<SwPreviousIncome>();
        List<PlafonHistory> plafonHistory = new ArrayList<PlafonHistory>();
        List<LimitTerpakai> limitTerpakaiList = new ArrayList<LimitTerpakai>();
        if (!isNewCustomer) {
            totalAngsuranList = financingHistoryService.getTotalAngsuran(sw.getCustomerId());
            previousIncomeList = financingHistoryService.getPreviousIncome(sw.getCustomerId());
            plafonHistory = financingHistoryService.getPlafonHistory(sw.getCustomerId());
            limitTerpakaiList = financingHistoryService.getLimitTerpakai(sw.getCustomerId());
        }
        // Fill email data begin
        /*
         * Miscellaneous / Parameter
         */
        val.setStatus(sw.getStatus());
        val.setKategoriNasabah(sw.getCustomerRegistrationType().toString());
        BigDecimal angsuran = sWProductMapping.getRecommendedInstallment() == null ? BigDecimal.ZERO : sWProductMapping.getRecommendedInstallment().divide(new BigDecimal(2), 4, RoundingMode.HALF_UP);
        ApprovalVariable appVar = new ApprovalVariable();
        BigDecimal angsuranTotal = BigDecimal.ZERO;
        if (isNewCustomer) {
            appVar = countVariable(sw, angsuran);
        } else {
            angsuranTotal = totalAngsuranList.isEmpty() ? BigDecimal.ZERO : totalAngsuranList.get(0).getTotalAngsuran();
            appVar = countVariable(sw, angsuran, angsuranTotal);
        }
        log.debug(" max angsuran " + appVar.getMaxAngsuran().intValue() + " max angsuran persen " + appVar.getMaxAngsuranPercent().intValue());
        /*
         * Header
         */
        val.setNamaSco(user.getName());
        val.setNamaNasabah(sw.getCustomerName());
        val.setNamaMms(location.getName());
        val.setKodeMms(location.getLocationCode());
        if (isNewCustomer) {
            val.setTahunKe("0");
            val.setBulanKe("0");
        } else {
            Customer customer = customerService.findById(sw.getCustomerId());
            Period yearAndDate = compareYearsAndMonths(customer.getCreatedDate());
            val.setTahunKe(String.valueOf(yearAndDate.getYears()));
            val.setBulanKe(String.valueOf(yearAndDate.getMonths()));
        }
        /*
         * Pengajuan Pembiayaan
         */
        val.setProduk(loanProduct.getProductName());
        val.setPlafon(formatCurrency(sWProductMapping.getRecommendedPlafon(), df));
        val.setTenor(loanProduct.getTenorBulan().toString());
        val.setAngsuran(formatCurrency(angsuran, df));
        val.setAngsuranTotal(formatCurrency(angsuranTotal, df));
        val.setMargin(formatPercent(loanProduct.getMargin(), df));
        if (sw.getDisbursementDate() != null) {
            val.setTglCair(sdfGlobal.format(sw.getDisbursementDate()));
        } else {
            val.setTglCair("");
        }
        /*
         * Limit Pembiayaan
         */
        BigDecimal limitTerpakai = limitTerpakaiList.isEmpty() ? BigDecimal.ZERO : limitTerpakaiList.get(0).getRemainingPrincipal();
        if (appVar.getMaxAngsuran().compareTo(BigDecimal.ZERO) == 0) {
            val.setLimitPembiayaanTotal(formatCurrency(BigDecimal.ZERO, df));
            val.setLimitTerpakai(formatCurrency(limitTerpakai, df));
            val.setLimitTersisa(formatCurrency(BigDecimal.ZERO, df));
        } else {
            List<PlafonMapping> plafonMapping = plafonMappingService.findByMonthlyInstallment(appVar.getMaxAngsuran().intValue(), loanProduct.getTenorBulan().intValue());
            if (plafonMapping.isEmpty()) {
                val.setLimitPembiayaanTotal(formatCurrency(BigDecimal.ZERO, df));
                val.setLimitTerpakai(formatCurrency(limitTerpakai, df));
                val.setLimitTersisa(formatCurrency(BigDecimal.ZERO, df));
            } else {
                val.setLimitPembiayaanTotal(formatCurrency(plafonMapping.get(0).getPlafon(), df));
                val.setLimitTerpakai(formatCurrency(limitTerpakai, df));
                val.setLimitTersisa(formatCurrency(plafonMapping.get(0).getPlafon().subtract(limitTerpakai), df));
            }
        }
        /*
         * Rasio
         */
        if (isNewCustomer) {
            val.setTotalPlafonAktif(formatPercent(BigDecimal.ZERO, df));
            val.setTotalSaldoTabungan(formatPercent(BigDecimal.ZERO, df));
        } else {
            if (isZeroPbu) {
                val.setTotalPlafonAktif(formatPercent(BigDecimal.ZERO, df));
                val.setTotalSaldoTabungan(formatPercent(BigDecimal.ZERO, df));
            } else {
                val.setTotalPlafonAktif(formatPercent(countTotalActivePlafon(plafonHistory, sWProductMapping, sw), df));
                val.setTotalSaldoTabungan(formatPercent(BigDecimal.ZERO, df)); // tabungan not available
            }
        }
        /*
         * Riwayat Pembiayaan - Pembiayaan Utama dan Tambahan
         */
        if (!isNewCustomer) {
            List<FinancingHistory> historyList = financingHistoryService.getFinancingHistory(sw.getCustomerId());
            List<FinancingHistory> topup = financingHistoryService.getTopupHistory(sw.getCustomerId());
            List<FinancingHistory> konsumtif = financingHistoryService.getKonsumtifHistory(sw.getCustomerId());
            // Riwayat Pembiayaan Utama
            // Isi riwayat lain nya dengan 0 jika riwayat pembayaran hanya ada 1 atau 2
            if (historyList.isEmpty()) {
                val.setBiayaAkhir(getDefaultFinancingHistory(df));
                val.setBiayaSblmAkhir(getDefaultFinancingHistory(df));
                val.setBiayaSebelumY(getDefaultFinancingHistory(df));
            } else if (historyList.size() == 1) {
                val.setBiayaSblmAkhir(getDefaultFinancingHistory(df));
                val.setBiayaSebelumY(getDefaultFinancingHistory(df));
            } else if (historyList.size() == 2) {
                val.setBiayaSebelumY(getDefaultFinancingHistory(df));
            }
            // pengisian kolom riwayat pembiayaan utama
            int i = 0;
            for (FinancingHistory history : historyList) {
                RiwayatPembiayaanDto riwayatTerakhir = setFinancingHistory(history, df);
                if (i == 0) {
                    val.setBiayaAkhir(riwayatTerakhir);
                } else if (i == 1) {
                    val.setBiayaSblmAkhir(riwayatTerakhir);
                } else {
                    val.setBiayaSebelumY(riwayatTerakhir);
                }
                i++;
            }
            // riwayat topup
            if (!topup.isEmpty()) {
                for (FinancingHistory topups : topup) {
                    val.setBiayaTopup(setFinancingHistory(topups, df));
                }
            } else {
                val.setBiayaTopup(getDefaultFinancingHistory(df));
            }
            // rowayat konsumtif
            if (!konsumtif.isEmpty()) {
                for (FinancingHistory konsumtifs : konsumtif) {
                    val.setBiayaKonsumtif(setFinancingHistory(konsumtifs, df));
                }
            } else {
                val.setBiayaKonsumtif(getDefaultFinancingHistory(df));
            }
        }
        /*
         * Riwayat Tabungan (to be Confirm )
         */
        val.setSaldoTabungan(getDefaultSavingBalance());
        val.setFrek(getDefaultTransactionFrek());
        /*
         * Profil Usaha
         */
        if (businessTypeIds.contains(Long.parseLong(sw.getBusinessField()))) {
            BusinessType businessType = businessTypeService.findByBusinessId(Long.valueOf(sw.getBusinessField()));
            val.setJenisUsaha(businessType.getBusinessType());
        } else {
            val.setJenisUsaha(" ");
        }
        val.setDeskripsiUsaha(sw.getBusinessDescription());
        val.setWaktuOperasional(sw.getBusinessDaysOperation());
        val.setStatusTempatUsaha(setStatusTemptUsaha(sw.getBusinessWorkStatus().toString()));
        val.setLamaUsaha(String.valueOf((sw.getBusinessAgeYear() * 12) + sw.getBusinessAgeMonth()));
        /*
         * Perhitungan Pendapatan
         */
        val.setPendapatanPenjualan(formatCurrency(sw.getTotalIncome(), df));
        val.setPembelian(formatCurrency(sw.getTotalBuy(), df));
        val.setPengeluaranUsaha(formatCurrency(sw.getBusinessCost(), df));
        val.setPendapatanBersih(formatCurrency(appVar.getPendapatanBersih(), df));
        val.setPendapatanLainnya(formatCurrency(sw.getOtherIncome(), df));
        val.setPengeluaranNonUsaha(formatCurrency(sw.getNonBusinessCost(), df));
        val.setSisaPenghasilan(formatCurrency(appVar.getPendapatanBersih().subtract(sw.getNonBusinessCost()), df));
        val.setMaxAngsuran(formatCurrency(appVar.getMaxAngsuran(), df));
        val.setMaxAngsuranPersen(formatPercent(appVar.getMaxAngsuranPercent(), df));
        val.setIir(loanProduct.getIir().toString());
        /*
         * Pendapatan Sebelumnya
         */
        if (!previousIncomeList.isEmpty()) {
            SwPreviousIncome previousIncome = previousIncomeList.get(0);
            if (sw.getTotalIncome().compareTo(previousIncome.getTotalIncome()) == 0) {
                val.setIncomeDataIsSame(true);
                val.setKenaikanPendapatan(formatPercent(BigDecimal.ZERO, df));
                val.setPenurunanPendapatan(formatPercent(BigDecimal.ZERO, df));
            } else {
                if (isZeroPbu) {
                    val.setIncomeDataIsSame(true);
                    val.setKenaikanPendapatan(formatPercent(BigDecimal.ZERO, df));
                    val.setPenurunanPendapatan(formatPercent(BigDecimal.ZERO, df));
                } else {
                    val.setIncomeDataIsSame(false);
                    if (sw.getTotalIncome().compareTo(previousIncome.getTotalIncome()) == 1) {
                        if (previousIncome.getTotalIncome().compareTo(BigDecimal.ZERO) == 0) {
                            val.setKenaikanPendapatan(formatPercent(new BigDecimal(100), df));
                            val.setPenurunanPendapatan(formatPercent(BigDecimal.ZERO, df));
                        } else {
                            val.setKenaikanPendapatan(formatPercent(countKenaikanPendapatan(previousIncome, sw), df));
                            val.setPenurunanPendapatan(formatPercent(BigDecimal.ZERO, df));
                        }
                    } else {
                        val.setKenaikanPendapatan(formatPercent(BigDecimal.ZERO, df));
                        val.setPenurunanPendapatan(formatPercent(countPenurunanPendapatan(previousIncome, sw), df));
                    }
                }
            }
        } else {
            val.setIncomeDataIsSame(true);
            val.setKenaikanPendapatan(formatPercent(BigDecimal.ZERO, df));
            val.setPenurunanPendapatan(formatPercent(BigDecimal.ZERO, df));
        }
        // fill email data end
        return val;
    }

    /**
     * Check if total income is zero
     *
     * @param totalIncome total income
     * @return true if total income is zero
     * @author kusnendi
     */
    private boolean checkZeroTotalIncome(BigDecimal totalIncome) {
        if (totalIncome.compareTo(BigDecimal.ZERO) == 0) {
            return true;
        } else {
            return false;
        }
    }

    private boolean checkNewCustomer(String customerType) {
        if (customerType.equals("1")) {
            return true;
        } else {
            return false;
        }
    }

    private String setStatusTemptUsaha(String statusTempatUsaha) {
        if (statusTempatUsaha.equals("1")) {
            return WebGuiConstant.MILIK_PRIBADI;
        } else if (statusTempatUsaha.equals("2")) {
            return WebGuiConstant.MILIK_ORANG_TUA;
        } else if (statusTempatUsaha.equals("3")) {
            return WebGuiConstant.MILIK_PIHAK_KETIGA;
        } else {
            return WebGuiConstant.MILIK_LAINNYA;
        }
    }

    private BigDecimal countKenaikanPendapatan(SwPreviousIncome previousIncome, SurveyWawancara sw) {
        BigDecimal kenaikan = (sw.getTotalIncome().subtract(previousIncome.getTotalIncome())).divide(previousIncome.getTotalIncome(), 4, RoundingMode.HALF_UP);
        return kenaikan.multiply(new BigDecimal(100));
    }

    private BigDecimal countPenurunanPendapatan(SwPreviousIncome previousIncome, SurveyWawancara sw) {
        BigDecimal penurunan = (previousIncome.getTotalIncome().subtract(sw.getTotalIncome())).divide(previousIncome.getTotalIncome(), 4, RoundingMode.HALF_UP);
        return penurunan.multiply(new BigDecimal(100));
    }

    private BigDecimal countTotalActivePlafon(List<PlafonHistory> plafonHistory, SWProductMapping sWProductMapping, SurveyWawancara sw) {
        BigDecimal plafonHistoryTotal = BigDecimal.ZERO;
        if (!plafonHistory.isEmpty()) {
            for (PlafonHistory plafon : plafonHistory) {
                plafonHistoryTotal = plafonHistoryTotal.add(plafon.getPlafond());
            }
        }
        BigDecimal totalPlafornAktif = sWProductMapping.getRecommendedPlafon().add(plafonHistoryTotal);
        BigDecimal pendapatanPenjualan = sw.getTotalIncome().multiply(new BigDecimal(12));
        return (totalPlafornAktif.divide(pendapatanPenjualan, 4, RoundingMode.HALF_UP)).multiply(new BigDecimal(100));
    }

    private ApprovalVariable countVariable(SurveyWawancara sw, BigDecimal currentInstCost, BigDecimal totalInstCost) {
        BigDecimal installmentCost = totalInstCost == null ? currentInstCost : totalInstCost.add(currentInstCost);
        BigDecimal pendapatanBersih = sw.getTotalIncome().subtract(sw.getExpenditure());
        BigDecimal angsuran = pendapatanBersih.multiply(new BigDecimal(0.4));
        BigDecimal maxAngsuran = pendapatanBersih.compareTo(BigDecimal.ZERO) == 0 ? BigDecimal.ZERO : angsuran.subtract(installmentCost);
        BigDecimal maxAngsuranPersen = pendapatanBersih.compareTo(BigDecimal.ZERO) == 0 ? BigDecimal.ZERO : (installmentCost.divide(pendapatanBersih, 4, RoundingMode.HALF_UP)).multiply(new BigDecimal(100));
        return new ApprovalVariable(sw.getExpenditure(), pendapatanBersih, angsuran, maxAngsuran, maxAngsuranPersen);
    }

    private ApprovalVariable countVariable(SurveyWawancara sw, BigDecimal currentInstCost) {
        BigDecimal pendapatanBersih = sw.getTotalIncome().subtract(sw.getExpenditure());
        BigDecimal angsuran = pendapatanBersih.multiply(new BigDecimal(0.4));
        BigDecimal maxAngsuran = pendapatanBersih.compareTo(BigDecimal.ZERO) == 0 ? BigDecimal.ZERO : angsuran.subtract(currentInstCost);
        BigDecimal maxAngsuranPersen = pendapatanBersih.compareTo(BigDecimal.ZERO) == 0 ? BigDecimal.ZERO : (currentInstCost.divide(pendapatanBersih, 4, RoundingMode.HALF_UP)).multiply(new BigDecimal(100));
        return new ApprovalVariable(sw.getExpenditure(), pendapatanBersih, angsuran, maxAngsuran, maxAngsuranPersen);
    }

    private RiwayatPembiayaanDto setFinancingHistory(FinancingHistory financingHistory, DecimalFormat df) {
        List<MappingScoring> mappingScoring = financingHistoryService.getMappingScoring(financingHistory.getAppId());
        List<LoanPrs> loanPrsList = financingHistoryService.getLoanPrs(financingHistory.getLoanId());
        RiwayatPembiayaanDto riwayat = new RiwayatPembiayaanDto();
        riwayat.setPlafon(formatCurrency(financingHistory.getPlafond(), df));
        riwayat.setStatus(financingHistory.getStatus());
        if (!loanPrsList.isEmpty()) {
            for (LoanPrs loanPrs : loanPrsList) {
                riwayat.setOutstanding(formatCurrency(loanPrs.getOutstandingAmount(), df));
                riwayat.setAngsuran(formatCurrency(loanPrs.getCurrentMargin().add(loanPrs.getInstallmentAmount()), df));
                if (loanPrs.getOverdueDays() != null) {
                    riwayat.setDpd(loanPrs.getOverdueDays().toString());
                } else {
                    riwayat.setDpd("0");
                }
            }
        } else {
            riwayat.setOutstanding(formatCurrency(BigDecimal.ZERO, df));
            riwayat.setAngsuran(formatCurrency(BigDecimal.ZERO, df));
            riwayat.setDpd("0");
        }
        riwayat.setTenor(financingHistory.getTenorBulan() == null ? "0" : financingHistory.getTenorBulan().toString());
        riwayat.setProduk(financingHistory.getProductName());
        if (!mappingScoring.isEmpty()) {
            for (MappingScoring score : mappingScoring) {
                riwayat.setGrade(
                        getGrade(financingHistory.getTenorBulan() == null ? 0 : financingHistory.getTenorBulan(), score.getFrekuensiAbsen(), score.getTransaksiBaliout())
                );
            }
        } else {
            riwayat.setGrade("Tidak ada data grade");
        }
        return riwayat;
    }

    private String getGrade(int tenor, int absensiPrs, int danaSolidaritas) {
        if (tenor < 12) {
            return "Belum di Tentukan";
        } else if (tenor == 12) {
            if (absensiPrs <= 4) {
                if (danaSolidaritas <= 2) {
                    return "A";
                } else if (danaSolidaritas > 2 && danaSolidaritas <= 4) {
                    return "B";
                } else if (danaSolidaritas == 5) {
                    return "C";
                } else if (danaSolidaritas > 5) {
                    return "D";
                } else {
                    return "Undefinied";
                }
            } else if (absensiPrs > 4 && absensiPrs <= 9) {
                if (danaSolidaritas <= 2) {
                    return "B";
                } else if (danaSolidaritas > 2 && danaSolidaritas <= 4) {
                    return "B";
                } else if (danaSolidaritas == 5) {
                    return "C";
                } else if (danaSolidaritas > 5) {
                    return "D";
                } else {
                    return "Undefinied";
                }
            } else if (absensiPrs == 10) {
                if (danaSolidaritas <= 2) {
                    return "B";
                } else if (danaSolidaritas > 2 && danaSolidaritas <= 4) {
                    return "C";
                } else if (danaSolidaritas == 5) {
                    return "D";
                } else if (danaSolidaritas > 5) {
                    return "D";
                } else {
                    return "Undefinied";
                }
            } else if (absensiPrs > 10) {
                return "D";
            } else {
                return "Undefinied";
            }
        } else if (tenor > 12 && tenor <= 18) {
            if (absensiPrs <= 6) {
                if (danaSolidaritas <= 3) {
                    return "A";
                } else if (danaSolidaritas > 3 && danaSolidaritas <= 6) {
                    return "B";
                } else if (danaSolidaritas == 7) {
                    return "C";
                } else if (danaSolidaritas > 7) {
                    return "D";
                } else {
                    return "Undefinied";
                }
            } else if (absensiPrs > 6 && absensiPrs <= 14) {
                if (danaSolidaritas <= 3) {
                    return "B";
                } else if (danaSolidaritas > 3 && danaSolidaritas <= 6) {
                    return "B";
                } else if (danaSolidaritas == 7) {
                    return "C";
                } else if (danaSolidaritas > 7) {
                    return "D";
                } else {
                    return "Undefinied";
                }
            } else if (absensiPrs == 15) {
                if (danaSolidaritas <= 3) {
                    return "B";
                } else if (danaSolidaritas > 3 && danaSolidaritas <= 6) {
                    return "C";
                } else if (danaSolidaritas == 7) {
                    return "D";
                } else if (danaSolidaritas > 7) {
                    return "D";
                } else {
                    return "Undefinied";
                }
            } else if (absensiPrs > 15) {
                return "D";
            } else {
                return "Undefinied";
            }
        } else if (tenor > 18 && tenor <= 24) {
            if (absensiPrs <= 9) {
                if (danaSolidaritas <= 4) {
                    return "A";
                } else if (danaSolidaritas > 4 && danaSolidaritas <= 9) {
                    return "B";
                } else if (danaSolidaritas == 10) {
                    return "C";
                } else if (danaSolidaritas > 10) {
                    return "D";
                } else {
                    return "Undefinied";
                }
            } else if (absensiPrs > 9 && absensiPrs <= 19) {
                if (danaSolidaritas <= 4) {
                    return "B";
                } else if (danaSolidaritas > 4 && danaSolidaritas <= 9) {
                    return "B";
                } else if (danaSolidaritas == 10) {
                    return "C";
                } else if (danaSolidaritas > 10) {
                    return "D";
                } else {
                    return "Undefinied";
                }
            } else if (absensiPrs == 20) {
                if (danaSolidaritas <= 4) {
                    return "B";
                } else if (danaSolidaritas > 4 && danaSolidaritas <= 9) {
                    return "C";
                } else if (danaSolidaritas == 10) {
                    return "D";
                } else if (danaSolidaritas > 10) {
                    return "D";
                } else {
                    return "Undefinied";
                }
            } else if (absensiPrs > 20) {
                return "D";
            } else {
                return "Undefinied";
            }
        } else {
            return "Undefinied";
        }
    }

    /**
     * Check if request is already approved / rejected / returned
     *
     * @param status status of the request
     * @return whatever the request is already approved or not
     * @author kusnendi
     */
    private boolean isAlreadyDecided(String status) {
        if (status.equals(WebGuiConstant.STATUS_WAITING_APPROVAL)) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Error page for already approved / rejected / returned request
     *
     * @param swStatus   request status
     * @param modifiedBy user who previously approved / rejected request
     * @param model      for adding attribute
     * @return already decided error page path
     * @author kusnendi
     */
    private String getAlreadyDecidedErrorPage(String swStatus, String modifiedBy, Model model) {
        if (WebGuiConstant.STATUS_APPROVED.equals(swStatus)) {
            model.addAttribute("statusSw", "setujui");
        } else if (WebGuiConstant.STATUS_REJECTED.equals(swStatus)) {
            model.addAttribute("statusSw", "tolak");
        } else if (WebGuiConstant.STATUS_RETURNED.equals(swStatus)) {
            model.addAttribute("statusSw", "kembalikan");
        } else if (WebGuiConstant.STATUS_CLOSED.equals(swStatus)) {
            model.addAttribute("statusSw", "tutup");
        } else if (WebGuiConstant.STATUS_CANCEL.equals(swStatus)) {
            model.addAttribute("statusSw", "batalkan");
        } else {
            model.addAttribute("statusSw", swStatus);
        }
        model.addAttribute("modifiedBy", modifiedBy);
        return WebGuiConstant.MAIL_ERROR_ALREADY_APPROVED_PATH;
    }

    /**
     * Display general error page
     *
     * @param errorMessage message displayed on error page
     * @param model        for adding attribute
     * @return general error page path
     * @author kusnendi
     */
    private String getGeneralErrorPage(String errorMessage, Model model) {
        model.addAttribute("errorMsg", errorMessage);
        return WebGuiConstant.MAIL_ERROR_GENERAL_PAGE_PATH;
    }

    /**
     * Return error response
     *
     * @param response        response object
     * @param responseCode    error code
     * @param responseMessage error message
     * @return Error response
     * @author kusnendi
     */
    private MailResponse getGeneralErrorResponse(MailResponse response, String responseCode, String responseMessage) {
        response.setResponseCode(responseCode);
        response.setResponseMessage(responseMessage);
        return response;
    }

    /**
     * Set error response without return response object
     *
     * @param response        response object
     * @param responseCode    error code
     * @param responseMessage error message
     * @author kusnendi
     */
    private void setErrorResponse(MailResponse response, String responseCode, String responseMessage) {
        response.setResponseCode(responseCode);
        response.setResponseMessage(responseMessage);
    }

    /**
     * Make used token expired
     *
     * @param mailToken  token object
     * @param modifiedBy user who set token expired
     * @param statusSw   status of the token
     * @author kusnendi
     */
    private void setUsedToken(MailToken mailToken, String modifiedBy, String statusSw) {
        mailToken.setIsExpired("1");
        mailToken.setModifiedBy(modifiedBy);
        mailToken.setModifiedDate(new Date());
        mailToken.setStatus(statusSw);
        mailTokenService.save(mailToken);
    }

    /**
     * Pack all variable for approval request
     *
     * @param status      approval status
     * @param swId        customer swId
     * @param username    approval username
     * @param listHistory customer approval history
     * @return SWApprovalMailRequest object
     * @throws IOException
     * @throws JsonGenerationException
     * @throws JsonMappingException
     * @author kusnendi
     */
    private String approvalRequestPack(String status, String swId, String username, List<SWApprovalHistoryReq> listHistory) throws JsonMappingException, JsonGenerationException, IOException {
        SWApprovalMailRequest approvalRequest = new SWApprovalMailRequest();
        approvalRequest.setStatus(status);
        approvalRequest.setSwId(swId);
        approvalRequest.setUsername(username);
        approvalRequest.setHistory(listHistory);
        String approvalRequestStr = new JsonUtils().toJson(approvalRequest);
        return approvalRequestStr;
    }

    /**
     * Pack all variable for reject request
     *
     * @param swId         customer swId
     * @param username     approval username
     * @param rejectReason reason why this request rejected
     * @return SWApprovalMailRequest object
     * @throws IOException
     * @throws JsonGenerationException
     * @throws JsonMappingException
     * @author kusnendi
     */
    private String rejectRequestPack(String swId, String username, String rejectReason) throws JsonMappingException, JsonGenerationException, IOException {
        SWApprovalMailRequest approvalRequest = new SWApprovalMailRequest();
        approvalRequest.setStatus(WebGuiConstant.STATUS_REJECTED);
        approvalRequest.setSwId(swId);
        approvalRequest.setUsername(username);
        approvalRequest.setRejectedReason(rejectReason);
        String approvalRequestStr = new JsonUtils().toJson(approvalRequest);
        return approvalRequestStr;
    }

    /**
     * Pack all request for sending mail
     *
     * @param swId          customer swId
     * @param subject       mail subject
     * @param mailServerStr address of mail server
     * @param emailAddress  address of mail receiver
     * @param userName      co username
     * @param mailDomain    domain used for approval links
     * @param ktpPhoto      Foto ktp
     * @param surveyPhoto   Foto Survey
     * @return MailDto object
     * @author kusnendi
     */
    private MailDto mailPack(String swId, String subject, String mailServerStr, String emailAddress, String userName, String mailDomain, byte[] ktpPhoto, byte[] surveyPhoto) {
        MailDto mail = new MailDto();
        mail.setSwId(swId);
        mail.setSubject(subject);
        mail.setMailServer(mailServerStr);
        mail.setTo(emailAddress);
        mail.setUserName(userName);
        mail.setDomain(mailDomain);
        mail.setKtpPhoto(ktpPhoto);
        mail.setSurveyPhoto(surveyPhoto);
        return mail;
    }

    /**
     * Pack all request for sending notification mail
     *
     * @param subject       mail subject
     * @param emailAddress  address of mail receiver
     * @param mailServerStr address of mail server
     * @param recieverName  name of mail receiver
     * @return MailDto object
     * @author kusnendi
     */
    private MailDto mailPack(String subject, String emailAddress, String mailServerStr, String recieverName) {
        MailDto mail = new MailDto();
        mail.setSubject(subject);
        mail.setTo(emailAddress);
        mail.setMailServer(mailServerStr);
        mail.setReciverName(recieverName);
        return mail;
    }

    /**
     * Pack all request for sending notification mail if request rejecred by system
     *
     * @param subject       mail subject
     * @param emailAddress  reciever email address
     * @param mailServerStr sender email address
     * @param custName      customer name
     * @param appidNo       app Id number
     * @param sentra        sentra
     * @param alasan        reason why request rejected by system
     * @return RejectDto object
     * @author kusnendi
     */
    private RejectDto mailRejectPack(String subject, String emailAddress, String mailServerStr, String custName, String appidNo, String sentra, String alasan) {
        RejectDto reject = new RejectDto();
        reject.setSubject(subject);
        reject.setTo(emailAddress);
        reject.setMailServer(mailServerStr);
        reject.setCustName(custName);
        reject.setAppidNo(appidNo);
        reject.setSentra(sentra);
        reject.setAlasan(alasan);
        return reject;
    }

    /**
     * get default saving balance value
     *
     * @return default saving balance value
     * @author kusnendi
     */
    private SaldoTabunganDto getDefaultSavingBalance() {
        return new SaldoTabunganDto("0", "0", "0", "0");
    }

    /**
     * get default financing history value
     *
     * @return default financing history value
     * @author kusnendi
     */
    private RiwayatPembiayaanDto getDefaultFinancingHistory(DecimalFormat df) {
        RiwayatPembiayaanDto riwayat1 = new RiwayatPembiayaanDto();
        riwayat1.setAngsuran(formatCurrency(BigDecimal.ZERO, df));
        riwayat1.setGrade("-");
        riwayat1.setOutstanding(formatCurrency(BigDecimal.ZERO, df));
        riwayat1.setPlafon(formatCurrency(BigDecimal.ZERO, df));
        riwayat1.setProduk("-");
        riwayat1.setStatus("-");
        riwayat1.setTenor("-");
        riwayat1.setDpd("0");
        return riwayat1;
    }

    /**
     * get default transaction frequency value
     *
     * @return default transaction frequency value
     * @author kusnendi
     */
    private FrekwensiTransaksiDto getDefaultTransactionFrek() {
        return new FrekwensiTransaksiDto("0", "0", "0", "0", "0", "0", "0", "0");
    }

    /**
     * Get customer history and map to SWApprovalHistoryReq based of customer swId
     *
     * @param swId customer swId
     * @param username user name BC if forwarded
     * @param isForwarded set true for forwarded message
     * @return customer history, return null if not found
     */
    private List<SWApprovalHistoryReq> getUserBwmpMap(Long swId, String username,boolean isForwarded) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        List<SWUserBWMPMapping> mapList = sWService.findBySw(swId);
        List<SWApprovalHistoryReq> historyList = new ArrayList<SWApprovalHistoryReq>();
        if (!mapList.isEmpty()) {
            for (SWUserBWMPMapping map : mapList) {
                SWApprovalHistoryReq history = new SWApprovalHistoryReq();
                User user = userService.loadUserByUserId(map.getUserId());
                history.setDate(sdf.format(map.getCreatedDate()));
                history.setLevel(map.getLevel());
                history.setSupervisorId(String.valueOf(map.getUserId()));
                history.setSwId(String.valueOf(map.getSwId()));
                history.setLimit(user.getLimit());
                history.setName(user.getName());
                history.setRole(user.getRoleUser());
                historyList.add(history);
            }
        }else {
        	if(isForwarded) {
        		User user = userService.findUserByUsername(username);
        		SWApprovalHistoryReq history = new SWApprovalHistoryReq();
        		history.setDate(sdf.format(new Date()));
                history.setLevel("1");
                history.setSupervisorId(String.valueOf(user.getUserId()));
                history.setSwId(String.valueOf(swId));
                history.setLimit(user.getLimit());
                history.setName(user.getName());
                history.setRole(user.getRoleUser());
                historyList.add(history);
        	}
        }
        return historyList;
    }

    /**
     * Set Expired Date for link in email body <br />
     * After specified date expired, links no longer accessible
     *
     * @param dateParam    instance of time ( d = day, m= month, y = year ) default d
     * @param expiredParam value link should be expired based of dateParam
     * @return expired date of links
     * @author kusnendi
     */
    private Date setExpiredDate(String dateParam, String expiredParam) {
        int expired = Integer.parseInt(expiredParam);
        Calendar cal = Calendar.getInstance();
        if (dateParam.equalsIgnoreCase("d")) {
            cal.add(Calendar.DATE, expired);
        } else if (dateParam.equalsIgnoreCase("m")) {
            cal.add(Calendar.MONTH, expired);
        } else if (dateParam.equalsIgnoreCase("y")) {
            cal.add(Calendar.YEAR, expired);
        } else {
            cal.add(Calendar.DATE, expired);
        }
        return cal.getTime();
    }

    /**
     * Get Parameter from T_MAIL_PARAM needed for mail engine
     *
     * @return HashMap contains mail parameter
     * @author kusnendi
     */
    private HashMap<String, String> getMailParam() {
        HashMap<String, String> mailParamHash = new HashMap<String, String>();
        List<MailParameter> mailParamList = mailParamSvc.getAll();
        for (MailParameter mailParam : mailParamList) {
            mailParamHash.put(mailParam.getMessageParam(), mailParam.getMessageValue());
        }
        return mailParamHash;
    }

    /**
     * Check if links expired or not
     *
     * @param expiredDate date links should expired
     * @return true if today date passed the expiration date
     * @author kusnendi
     */
    private boolean isDateExpired(Date expiredDate) {
        Calendar cal = Calendar.getInstance();
        if (expiredDate.getTime() - cal.getTime().getTime() <= 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Calculate year between today and specified date
     *
     * @param date date that you want to compare the year
     * @return year between today and specified date
     * @author kusnendi
     */
    private Period compareYearsAndMonths(Date date) {
        LocalDate joinDateLocal = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        LocalDate today = LocalDate.now(ZoneId.systemDefault());
        return Period.between(joinDateLocal, today);
    }

    /**
     * Format BigDecimal into Rupiah (IDR)
     *
     * @param number Number in BigDecimal
     * @param df     decimal format
     * @return formatted string
     * @author kusnendi
     */
    private String formatCurrency(BigDecimal number, DecimalFormat df) {
        df.setDecimalFormatSymbols(symbolsRp);
        df.setMaximumFractionDigits(0);
        return df.format(number.longValue());
    }

    /**
     * Format BigDecimal into String with two digit decimal value
     *
     * @param number Number in BigDecimal
     * @param df     decimal format
     * @return Formatted String
     * @author Kusnendi
     */
    private String formatPercent(BigDecimal number, DecimalFormat df) {
        df.setDecimalFormatSymbols(symbolsPercent);
        df.setMaximumFractionDigits(2);
        return df.format(number);
    }

    /**
     * Format currency Symbols
     *
     * @param currencySymbols custom currency Symbols
     * @return DecimalFormatSymbols
     */
    private DecimalFormatSymbols setSymbols(String currencySymbols) {
        DecimalFormatSymbols dfs = new DecimalFormatSymbols();
        dfs.setCurrencySymbol(currencySymbols);
        dfs.setMonetaryDecimalSeparator(',');
        dfs.setGroupingSeparator('.');
        return dfs;
    }

    /**
     * Check if email address is valid or null
     *
     * @param email email address
     * @return true if address is valid
     * @author kusnendi
     */
    private boolean isValidEmail(String email) {
        if (email == null) {
            return false;
        } else {
            if (EMAIL_REGEX.matcher(email).matches()) {
                return true;
            } else {
                return false;
            }
        }
    }

    /**
     * Check if user role is CO or BM
     *
     * @param role role code
     * @return true if user is BM
     * @author kusnendi
     */
    private boolean checkBmRole(String role) {
        if ("2".equalsIgnoreCase(role)) {
            return true;
        } else {
            return false;
        }
    }
    
    /**
     * Check sw for DRAFT status
     * @param status status of sw 
     * @return true if status is DRAF
     * @author kusnendi
     */
    private boolean isDraf(String status) {
    	if(WebGuiConstant.STATUS_DRAFT.equalsIgnoreCase(status)) {
    		return true;
    	}else {
    		return false;
    	}
    }
    
    /**
     * Check if token is not expired
     * @param tokenStatus token expiration status
     * @return true if token is not expired
     * @author kusnendi
     */
    private boolean tokenNotExpired(String tokenStatus) {
    	if("1".equalsIgnoreCase(tokenStatus)) {
    		return false;
    	}else {
    		return true;
    	}
    }

}